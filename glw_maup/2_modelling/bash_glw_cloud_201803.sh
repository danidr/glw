#!/bin/bash
module purge
module load R/3.4.1-intel-2018

########################################################################################
## Run the models for CHICKEN with IRREGULAR BOUNDARIES UNITS and RANDOM SAMPLING points
########################################################################################
printf "########################################\n"
printf "## CH villages IRREGULAR random sampling\n"
printf "########################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/CHvil_IRRrp/MASTER.r > 03_Models/CHvil_IRRrp/CHvil_IRRrp.log 2> 03_Models/CHvil_IRRrp/CHvil_IRRrp.err & # & allows to work in parallel, while > allows to write log and error file 
printf "############################################\n"
printf "## CH subdistricts IRREGULAR random sampling\n"
printf "############################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/CHsds_IRRrp/MASTER.r > 03_Models/CHsds_IRRrp/CHsds_IRRrp.log 2> 03_Models/CHsds_IRRrp/CHsds_IRRrp.err &
printf "#########################################\n"
printf "## CH districts IRREGULAR random sampling\n"
printf "#########################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/CHdis_IRRrp/MASTER.r > 03_Models/CHdis_IRRrp/CHdis_IRRrp.log 2> 03_Models/CHdis_IRRrp/CHdis_IRRrp.err & 
########################################################################################
## Run the models for CHICKEN with REGULAR BOUNDARIES UNITS and RANDOM SAMPLING points
########################################################################################
printf "############################################\n"
printf "## CH subdistricts REGULAR random sampling\n"
printf "############################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/CHsds_REGrp/MASTER.r > 03_Models/CHsds_REGrp/CHsds_REGrp.log 2> 03_Models/CHsds_REGrp/CHsds_REGrp.err &
printf "#########################################\n"
printf "## CH districts REGULAR random sampling\n"
printf "#########################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/CHdis_REGrp/MASTER.r > 03_Models/CHdis_REGrp/CHdis_REGrp.log 2> 03_Models/CHdis_REGrp/CHdis_REGrp.err &
########################################################################################
## Run the models for DUCK with IRREGULAR BOUNDARIES UNITS and RANDOM SAMPLING points
########################################################################################
printf "########################################\n"
printf "## DK villages IRREGULAR random sampling\n"
printf "########################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/DKvil_IRRrp/MASTER.r > 03_Models/DKvil_IRRrp/DKvil_IRRrp.log 2> 03_Models/DKvil_IRRrp/DKvil_IRRrp.err & 
printf "############################################\n"
printf "## DK subdistricts IRREGULAR random sampling\n"
printf "############################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/DKsds_IRRrp/MASTER.r > 03_Models/DKsds_IRRrp/DKsds_IRRrp.log 2> 03_Models/DKsds_IRRrp/DKsds_IRRrp.err &
printf "#########################################\n"
printf "## DK districts IRREGULAR random sampling\n"
printf "#########################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/DKdis_IRRrp/MASTER.r > 03_Models/DKdis_IRRrp/DKdis_IRRrp.log 2> 03_Models/DKdis_IRRrp/DKdis_IRRrp.err & 
########################################################################################
## Run the models for DUCK with REGULAR BOUNDARIES UNITS and RANDOM SAMPLING points
########################################################################################
printf "############################################\n"
printf "## DK subdistricts REGULAR random sampling\n"
printf "############################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/DKsds_REGrp/MASTER.r > 03_Models/DKsds_REGrp/DKsds_REGrp.log 2> 03_Models/DKsds_REGrp/DKsds_REGrp.err &
printf "#########################################\n"
printf "## DK districts REGULAR random sampling\n"
printf "#########################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/DKdis_REGrp/MASTER.r > 03_Models/DKdis_REGrp/DKdis_REGrp.log 2> 03_Models/DKdis_REGrp/DKdis_REGrp.err &
########################################################################################
## Run the models for CHICKEN with IRREGULAR BOUNDARIES UNITS and AVERAGED sampling 
########################################################################################printf "########################################\n"
printf "## CH villages IRREGULAR averaged sampling\n"
printf "########################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/CHvil_IRRav/MASTER.r > 03_Models/CHvil_IRRav/CHvil_IRRav.log 2> 03_Models/CHvil_IRRav/CHvil_IRRav.err & # & allows to work in parallel, while > allows to write log and error file 
printf "############################################\n"
printf "## CH subdistricts IRREGULAR averaged sampling\n"
printf "############################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/CHsds_IRRav/MASTER.r > 03_Models/CHsds_IRRav/CHsds_IRRav.log 2> 03_Models/CHsds_IRRav/CHsds_IRRav.err &
printf "#########################################\n"
printf "## CH districts IRREGULAR averaged sampling\n"
printf "#########################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/CHdis_IRRav/MASTER.r > 03_Models/CHdis_IRRav/CHdis_IRRav.log 2> 03_Models/CHdis_IRRav/CHdis_IRRav.err & 
########################################################################################
## Run the models for CHICKEN with REGULAR BOUNDARIES UNITS and AVERAGED sampling 
########################################################################################
printf "############################################\n"
printf "## CH subdistricts REGULAR averaged sampling\n"
printf "############################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/CHsds_REGav/MASTER.r > 03_Models/CHsds_REGav/CHsds_REGav.log 2> 03_Models/CHsds_REGav/CHsds_REGav.err &
printf "#########################################\n"
printf "## CH districts REGULAR averaged sampling\n"
printf "#########################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/CHdis_REGav/MASTER.r > 03_Models/CHdis_REGav/CHdis_REGav.log 2> 03_Models/CHdis_REGav/CHdis_REGav.err &
########################################################################################
## Run the models for DUCK with IRREGULAR BOUNDARIES UNITS and AVERAGED sampling 
########################################################################################
printf "########################################\n"
printf "## DK villages IRREGULAR averaged sampling\n"
printf "########################################\n"
time Rscript ~/GLW_test/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/DKvil_IRRav/MASTER.r > 03_Models/DKvil_IRRav/DKvil_IRRav.log 2> 03_Models/DKvil_IRRav/DKvil_IRRav.err & 
printf "############################################\n"
printf "## DK subdistricts IRREGULAR averaged sampling\n"
printf "############################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/DKsds_IRRav/MASTER.r > 03_Models/DKsds_IRRav/DKsds_IRRav.log 2> 03_Models/DKsds_IRRav/DKsds_IRRav.err &
printf "#########################################\n"
printf "## DK districts IRREGULAR averaged sampling\n"
printf "#########################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/DKdis_IRRav/MASTER.r > 03_Models/DKdis_IRRav/DKdis_IRRav.log 2> 03_Models/DKdis_IRRav/DKdis_IRRav.err & 
########################################################################################
## Run the models for DUCK with REGULAR BOUNDARIES UNITS and AVERAGED sampling 
########################################################################################
printf "############################################\n"
printf "## DK subdistricts REGULAR averaged sampling\n"
printf "############################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/DKsds_REGav/MASTER.r > 03_Models/DKsds_REGav/DKsds_REGav.log 2> 03_Models/DKsds_REGav/DKsds_REGav.err &
printf "#########################################\n"
printf "## DK districts REGULAR averaged sampling\n"
printf "#########################################\n"
time Rscript ~/GLW_test/20180207/MultipleGLWCountry_Models/03_Models/DKdis_REGav/MASTER.r > 03_Models/DKdis_REGav/DKdis_REGav.log 2> 03_Models/DKdis_REGav/DKdis_REGav.err &