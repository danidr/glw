#02.11.2017
#Daniele Da Re
#UCL & ULB

#Instruction to run the multiple model

#Code, Predictors and Respons Variables directory will not change and remain into ah higher hierachical level than the moldels path.

#Open linux terminal and type: 
#~$ sh bash_glw_tes1.sh

#the bash file will run each R MASTER.r script, producing different models results.

#Model names: 
	CH (= chickens), DK (= ducks)
	vil (= villages census level), sds (= sub districts census level), dis (= districts census level) 
	IRR (= irregular sample unit boundaries), REG(= regular sample unit boundaries)
	rp (= random sampling), ag(= aggregating sampling)

#e.g.: "CHvil_IRRrp" means: chicken count data at villages census level with irreglar sample unit boundaries, sampled using random sampling within each sampling units. 

#List of model for chickens
#CHvil_IRRrp	CHvil_IRRag
#CHsds_IRRrp	CHsds_IRRag
#CHdis_IRRrp	CHdis_IRRag

#CHvil_REGrp	CHvil_REGag
#CHsds_REGrp	CHsds_REGag
#CHdis_REGrp 	CHdis_REGag

#List of model for ducks
#DKvil_IRRrp	DKvil_IRRag
#DKsds_IRRrp	DKsds_IRRag
#DKdis_IRRrp	DKdis_IRRag

#DKvil_REGrp	DKvil_REGag
#DKsds_REGrp	DKsds_REGag
#DKdis_REGrp 	DKdis_REGag

###Imp: in the predictors list .csv file, the predictors name should not exceed 10 characters



