###################################################################
## GLW 4.0 Country-level model
###################################################################
# Oct 2017
# Marius Gilbert (marius.gilbert@gmail.com)
###################################################################
# For the future
# - reinsert the possibility of workspace exrtaction
# - deal with projected data throughout

###################################################################
## Section 1. Setting the run parameters
###################################################################

# Absolute path of GLW
  P_GenPath = "/home/elic/ddare/GLW_test/20180207/MultipleGLWCountry_Models/"
# General folder of the run
  P_myOutData = "03_Models/CHdis_IRRav"
# General name of the run
  P_myOutName = "Ch"
# Maximum predicted density
  P_myMaxDens = 500000
# Temp folders to be used as scratch (local)
  P_TempFolder = "0_temp"
# Temp folders to be used as outputs
  P_OutputFolder = "1_outputs"
# File containing the list of predictors
  P_myRasterList = "/home/elic/ddare/GLW_test/20171207/MultipleGLWCountry_Models/02_Predictors/02_Predictors/Thailand_predictors_1.csv"
# Name of the Shape file (without extension) containing the livestock data
  P_myDepShape = "/home/elic/ddare/GLW_test/20171207/MultipleGLWCountry_Models/01_Response/01_Response/CHsdis_IRRrp"
# Name of the field containing a the unique numerical identifier by polygon
  P_myID = "ID"
# Name of the field containing the animal number per polygon
  P_myAnField = "count"
# Name of the field containing the geometric area of the polygon
  P_myArField = "area_km"
# What is the nodata value
  P_myNoLiData = -9999
  # 2.3.5 Should the Idraster conversion be redone or are the existing files
  # to be used ? (1: redo conversion; 0: leave as is)
  P_RedoIdRaster = 0
  # 2.4.1 Minimum number of sampling point per input polygon (note that different bootstrap will select different polygons)
  P_NsPol = 1
  # 2.4.2 Minimum density of sampling points per 10,000 square km of land per bootstrap
  P_Ns = 10
  # 2.3.6 What is the field where human population data is in the polygon 
  # input file ? If set to "None", will be ignored and human pop will be 
  # estimated from the Hpop raster
  P_HpopField = "None"
  # 2.4.4 Minimum size in km2 of polygons to which suitability correction should be applied
  P_MinSize = 25
  # 1.3.3 Should the zero values be used in the modelling
  P_UseZeros = 1
  # 2.5.3 Should the suitability mask be applied to the predictions (1: yes; 0: no)
  P_UseSuitMask = 1
  # 2.5.4 Should the administrative mask be used (1: yes; 0: no)
  P_UseAdminMask = 0
  # 2.4.6 What proportion of sampling polygons should be used for training the model
  P_myModelSetProp = 0.7
  # 2.4.6 Should one use sampling (1) or aggregate predictors by polygons (0)
  P_UsePointSampling = 1
  # 2.3.7 What type of dependent variable should be used and modelled ?
  # (1: Suitability corrected density, 2: density, 3: number per capita, 4: raw number)
  P_TypeDependent = 1
  # 1.3.6 Should the input be corrected by maximum density
  P_CorrectInputByMax = 1  
  # 2.4.5 Number of bootstraps per sample file used for the predictions
  P_nBoot = 20
  # 2.5.8 Should the animal density of sampling points falling into a unsuitable area be set as 0 for the modelling (1: yes; 0: no)
  P_ForceUnsuitable = 1
  # 2.5.10 RF: factor by which the number of variables should be divided for building each tree
  P_RF_VarFactor_F = 3
  # 2.5.11 RF: Minimum number of variables to include for each tree
  P_RF_VarFactor_Min = 7
  # 2.5.11 RF: factor by which the number of points should be divided to get the number of trees
  P_RF_Ntrees_F = 20
  # 2.5.12 RF: Minimum number of tress
  P_RF_Ntrees_Min = 500
  # 2.5.13 RF: factor by which the number of points should be divided to get the node size
  P_RF_NodeSize_F = 1000
  # 2.5.14 RF: Minimum node size
  P_RF_NodeSize_Min = 5
  # 2.5.14 RF: Use unique values for the dependent variable ?
  P_UseUniqueValues = 0
  # 2.5.9 Are the predictions logged or not (1: yes; 0: no)
  P_PredictionAreLogged = 1
  # 2.1.7 Path for gis and figures output files
 # P_GIS_Folder = "2_gis"
  # 2.5.1 What is the minimum predicted density 
  P_myMinDens = 0
  

###################################################################
## Section 2. Launching the runs
###################################################################

# 2.1 Run the pre-processing scripts
###################################################################
#setwd(P_GenPath)
source(paste(P_GenPath, "00_Codes/GLW_0_Libraries.r", sep = ""))  
source(paste(P_GenPath, "00_Codes/GLW_1_CheckSampleInputs.r", sep = ""))
source(paste(P_GenPath, "00_Codes/GLW_2_CheckRasterInputs.r", sep = ""))
source(paste(P_GenPath, "00_Codes/GLW_3_CreateIdRaster.r", sep = ""))
source(paste(P_GenPath, "00_Codes/GLW_4_CreateMaster.r", sep = ""))
source(paste(P_GenPath, "00_Codes/GLW_5_CheckModelInputs.r", sep = ""))

# 2.2 Run the sampling and modelling
###################################################################
source(paste(P_GenPath, "00_Codes/GLW_6_CreateIndividualSamples.r", sep = ""))    
source(paste(P_GenPath, "00_Codes/GLW_7_Models_RF.r", sep = ""))
source(paste(P_GenPath, "00_Codes/GLW_8_Predictions_RF.r", sep = ""))

# 2.3 Run the post-processing and goodness of fit
###################################################################
source(paste(P_GenPath, "00_Codes/GLW_9_PostProcessing_PolygonEstimates.r", sep = ""))
source(paste(P_GenPath, "00_Codes/GLW_10_PostProcessing_Averaging.r", sep = ""))
source(paste(P_GenPath, "00_Codes/GLW_11_FittingMetrics_RF_Poly.r", sep = ""))

  
# 
# 
# ###################################################################
# ## Section 1. Main run parameters (to be checked at each new run)
# ###################################################################
# 
#   # 1.1 General path and characteristics
#   #################################################################
#   # 1.1.0 Absolute path of GLW
#       #P_GenPath = "/Users/mgilbert/Documents/LargeDB/GLW"
#       P_GenPath = "/export/data/ilri/glw"
#       # 1.1.1 General folder of the run
#       P_myOutData = "1_Glw33/1k_ch_th"
#       # 1.1.2 General name of the run
#       P_myOutName = "Ch"
#       # 1.1.3 Species being modelled
#       P_myOutTitle = "Chicken"
#       # 1.1.4 Species icon
#       P_myOutIcon = "1_Global/Census/Ch/CH.png"
#       
#   # 1.2 Input shape files and fields
#   #################################################################
#     # 1.2.1 Name of the Shape file (without extension) containing the livestock data
#       P_myDepShape = "1_Global/Census/Ch/Ch_170609_Wd"
#     # 1.2.2 Name of the field containing the animal number per polygon
#       P_myAnField = "CHN0"
#     # 1.2.2 Name of the field containing the animal number per polygon
#       P_myAnYear = "CH_YR"
#     # 1.2.2 Name of the field containing the FAOSTAT number
#       P_myAnCCField = "CHALN10"
#     # 1.2.3 Name of the field containing the FAOSTAT number
#       P_myFaostatYear = "2010"
#       
#   # 1.3 Modelling parameters
#   #################################################################
#     # 1.3.1 Maximum predicted density
#       P_myMaxDens = 500000
#     # 1.3.1 Maximum predicted density
#       P_PerCapitaMax = 1000
#     # 1.3.2 Small polygon correction (8k: 1 - 1k: 0)
#       P_ApplySmallPolygonCorrection = 0
#     # 1.3.4: Use FAOSTAT corrections prior to modelling (1: Yes, 0: No)
#       P_CorrectSampleByFAOSTAT = 1
#     # 1.3.5 Should the zero values be used in the polygon correction
#       P_UseZeros_PolygonCorrection = 0
#     # 1.3.5 Should the zero values be used in the country correction
#       P_UseZeros_CountryCorrection = 1
# 
#             
# ###################################################################
# ## Section 2. Advanced parameters (unfrequent changes)
# ###################################################################
# 
#   # 2.1 Sets the general path, the number of cores to be used and scratch path
#   #################################################################
#     # 2.1.1 Number of cores to be used (used on ILRI cluster only)
#       P_ncores = 5
#     # 2.1.2 Temp folders to be used as scratch (local)
#       P_TempFolder = "2_Temp"
#     # 2.1.3 Temp folders to be used as scratch (ILRI cluster)
#       args = commandArgs(trailingOnly = F)
#       myscratchpath = args[length(args)]
#       P_TempFolder = paste(myscratchpath,"/",sep="")
#     # 2.1.4 Path for storing semi-temporary files
#       P_STempFolder = "0_temp"
#     # 2.1.5 Path for run input quality checks files
#       P_InQC_Folder = "1_qc"
#     # 2.1.6 Path for run output quality checks files
#       P_OutQC_Folder = "1_qc"
#     # 2.1.7 Path for gis and figures output files
#       P_GIS_Folder = "2_gis"
# 
#   # 2.2 Sets the bounding box of the study region and define extraction
#   #################################################################
#     # 2.2.1 Global run ? (0: No - 1: Yes)
#       P_IsGlobal = 0
#     # 2.2.2 Bounding box of the study region if not a global run
#       P_XMin = -18
#       P_XMax = 52
#       P_YMin = -35
#       P_YMax = 38
#     # 2.2.3 Folder where extracted images will be placed (if not a global run)
#       P_myExFolder = "1_Glw33/0_Workspace1k"
#     # 2.2.4 Extracted images prefix (if not a global run)
#       P_myImPrefix = "Af"
#     # 2.2.5 Should the image extractions forced to be redone (0: No - 1: Yes)
#       P_RedoExtraction = 0
#     # 2.2.6 Overwrite existing files during extractions
#       P_OverWriteCrop = T
#     # 2.2.7 Factor by which each pixel values has to be divided to obtain km2 per pixel
#       P_myAreaFactor = 1
#     # 2.2.8 Use suitability corrected densities for the ArealWeighted method
#       P_ArealWeightedCorrected = 0
#       
# 
#   # 2.3 Input files and fields
#   #################################################################
#     # 2.3.1 File containing the list of predictos
#       P_myRasterList = "1_Glw33/0_Varlists/1k_FP_Ensemble_oldmgmask_20160606.csv"
#     # 2.3.2 Name of the field containing a the unique numerical identifier by polygon
#       P_myID = "ID"
#     # 2.3.3 Name of the field containing the geometric area of the polygon
#       P_myArField = "AREAKM"
#     # 2.3.4 What is the nodata value
#       P_myNoLiData = -9999
#     # 2.3.5 Should the Idraster conversion be redone or are the existing files
#     # to be used ? (1: redo conversion; 0: leave as is)
#       P_RedoIdRaster = 0
# 
#     # 2.3.8 Path to the global shapefile (no extension) with simple countries boundaries
#       P_myGlobalShape = "1_Global/GAUL/country_worldfao6"
#     # 2.3.9 Path to the file with css style for html reports
#       P_CSS = "0_Program/2.3/R2HTML.css"
#     # 2.3.10 Name of the Shape file (without extension) containing the national livestock FAOSTAT data
#       P_myCountryShape = "1_Global/FAOSTAT/WORLDFAO6"
#     # 2.3.11 Name of the field containing the country numerical ID
#       P_myIDCC = "ID"  
#   
#   # 2.4 Sampling Options
#   #################################################################
#     # 2.4.3 Levels of the stratification layer to be used
#       P_myLevels = c("1","2","3","4","5","6")
#     # 2.4.7 What proportion of points within polygons should be used for training the model
#       P_myModelSetPointProp = 0.5
#     # 2.4.8 Names of the stratification layer (from 1 to 7)
#       P_myLevelsNames = c("Africa","Asia","Oceania","Europe","North. Am.","South. Am.")
#       
#   # 2.5 Modelling options
#   ###################################################################
#     # 2.5.2 What is the NA code to be used in IDRISI Outputs
#       P_myNAVal = -1
#     # 2.5.4 Should the administrative mask be used (1: yes; 0: no)
#       P_UseAdminMask = 1
#     # 2.5.5 Should predictions be made where there was no training data ? (1:yes; 0: no)
#       P_FillGaps = 1
#     # 2.5.6 Below which polygon size should the observed densities replace the suitability corrected ones in inputs 
#       P_MinPolygonSizeKm_Input = 50
#     # 2.5.7 Below which polygon size should the observed densities replace the modelled ones 
#       P_MinPolygonSizeKm = 250
#     # 2.5.14 RF: Should the model add a jitter to allow RF models with several points / polygon
#       P_RandomJitter = 0
#       
# 
#   # 2.6 Post-processing options
#   ###################################################################
#       P_PolCorrection_Type = 1
#     # 2.6.1 Number of iterations of the exp. polygon correction
#       P_PolCor_iter_poly = 9
#     # 2.6.2 Number of iterations of the exp. country correction
#       P_PolCor_iter_country = 6
#     # 2.6.3 Threshold used for per capita corrections
#       P_PolCorrection_Threshold = 100
#     # 2.6.4 Per Capita Corrections
#       P_PerCapitaCorrection = 0
#     # 2.6.5 List of specific extent for input/output quality checks
#       P_DisplayExtents = list(c(-145,-50,5,70),c(-18,50,23,60))
#     # 2.6.6 Export type (1: mean; 2: mean & polygon corrections; 3: mean, polygon correction & country-correction)
#       P_Export_type = 3
#     # 2.6.7 Export variable (1: density; 2: absolute number; 3: all)
#       P_Export_var = 2
#     # 2.6.8 Export transform (1: log-transformed; 2: raw numbers)
#       P_Export_var_transform = 2
#     # 2.6.9 List of specific extent for exports
#       P_ExportsExtents = list(c(-18,52,-35,38),c(19,180,-12,85),c(-25,34,27,81),c(-180,-50,5,85),c(-95,-30,-60,15),c(110,180,-54,0))
#     # 2.6.10 Names of specific prefix for exports
#       P_Exportsnames = c("Af","As","Eu","Na","Sa","Oc") 
# 
# 
