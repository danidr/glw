##########################################################
# Check Inputs prior to the extractuion of images
##########################################################
# Description: Inout check
##########################################################
# By: Marius Gilbert (mgilbert@ulb.ac.be)
##########################################################
# 23rd Sep. 2011
##########################################################

# Plot a global map with the extraction region
##############################################
# Loads & plot Global Shape
# myGlobalShape = readShapePoly(P_myGlobalShape, proj4string=CRS("+proj=longlat"))
# plot(myGlobalShape, main = "Extraction area", axes = T, ylim = c(-100,100))
# 
# # Plot Bounding box
# if (P_IsGlobal == 1) {
#   rect(-180,-90,180,90,
#        ylim = c(-90,90),
#        density = 10, col = "red", angle = 45,border = "red", lwd = 1.5)  
#   print("## Info: This is a global run with full extent  ")
#   
#   
# } else {  
# rect(P_XMin,P_YMin,P_XMax,P_YMax,
#      ylim = c(-60,70),
#      density = 10, col = "red", angle = 45,border = "red", lwd = 1.5)
# }

print("#####################################################")
print("## Checking workspace extraction inputs  ")
print("#####################################################")

# Check that all images are present and include
# the bounding box
###############################################
myImTable = subset(G_myPredTable, EXTRACTION == 1)
HasAllImages = 1
for (myRec in 1:nrow(myImTable)){
  Test = file.access(myImTable$PATH[myRec], mode = 4)[[1]]
  if (Test == -1) {
    HasAllImages = 0
    print(paste("The file ", myImTable$PATH[myRec], " is missing. No data will be extracted"))
  }
}

if (HasAllImages == 1) {
  print("## Info: All listed images are present in input folder   ")
} else {
  print("## Error: A file was missing, please check your inputs    ")
}

# HasAllExtent = 1
# for (myRec in 1:nrow(myImTable)){
#   Test = file.access(myImTable$PATH[myRec], mode = 4)[[1]]
#   if (Test == 0 ) {
#     myHeader = L_GetHeader (myImTable$PATH[myRec])
#     myImageisOK = 1
#     if (P_XMin < myHeader[5]) {myImageisOK = 0}
#     if (P_XMax > myHeader[8]) {myImageisOK = 0}
#     if (P_YMin < myHeader[6]) {myImageisOK = 0}
#     if (P_YMax > myHeader[9]) {myImageisOK = 0}
#     if (myImageisOK == 0) {
#       HasAllExtent = HasAllExtent + 1
#       print(paste("## Warning: the coordinates of the file ", myImTable$PATH[myRec]))
#       print("## do not fit the extraction area")
#     }
#   }
# }
# if (HasAllExtent == 1) {
#   print("## Info: The extraction extent is included in all images ")
# } else {
#   print("## Error: The extent is not covered by some images, check your inputs ")
# }

# MissingImages = 0
# MissingImagesVec = c()
# 
# for (myRec in 1:nrow(myImTable)){
#   Test = file.access(myImTable$EXPATH[myRec], mode = 4)[[1]]
#   if (Test == -1) {
#     MissingImages = MissingImages + 1
#     MissingImagesVec = c(MissingImagesVec,myRec)
#   }
# }
# if (MissingImages > 0 ) {
#   print("#####################################################")
#   print("## Some images have not yet been extracted, and will be extracted now")
# } else {
#   print("#####################################################")
#   print("## All images have already been extracted successfuly  ")
# }

print("#####################################################")
print("## End of check of raster workspace verification  ")
print("#####################################################")





