##########################################################
# Create the Master sampling file from where
# Individual sample files are built
##########################################################

myTime = date()
print("#############################################################")    
print(paste("## Creating Master sample files: starting on ", myTime))
print("#############################################################")    

#################################f####################################
# Open the shape file if it is required and calculate the
# corrected area ratio
#####################################################################
  print("## Evaluating final number of points per polygons")
  myShapeDBF = read.dbf(paste(P_myDepShape,".dbf", sep = ""))
  myShape = readShapePoly(P_myDepShape,proj4string=CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"))
  myCoords = coordinates(myShape)
  myShapeDBF$x = myCoords[,1]
  myShapeDBF$y = myCoords[,2]
  myAreaDB = L_CalcSuitAreaRaster (G_mySuitMaskPath, G_myDepIdPath, G_myAreaPath,myShapeDBF[,P_myID])
  myShapeDBF$AreaKmC = myAreaDB[,1]
  myShapeDBF$SAreaKmC = myAreaDB[,2]
  myShapeDBF$Ratio = myAreaDB[,3]
  myShapeDBF$PixNum = myAreaDB[,4]
  myMaxArea = (max(myAreaDB[,1]/myAreaDB[,4], na.rm = T))*2
  myShapeDBF$PixNum = ifelse(is.na(myShapeDBF$PixNum),ifelse(myShapeDBF[,P_myArField]< myMaxArea,1,NA),myShapeDBF$PixNum)
  myShapeDBF$Ratio = ifelse(is.na(myShapeDBF$Ratio),1,myShapeDBF$Ratio)                         
  myShapeDBF$NAreaKm = myShapeDBF[,P_myArField]
  myShapeDBF$Npd = round((myShapeDBF$NAreaKm/10000)* P_Ns)
  myShapeDBF$Npd = ifelse(is.na(myShapeDBF$Npd),0, myShapeDBF$Npd)
  myShapeDBF$Npp = P_NsPol
  myShapeDBF$Npa = ifelse(myShapeDBF$Npd < P_NsPol, P_NsPol,myShapeDBF$Npd)
  myShapeDBF$PropPix = myShapeDBF$Npa / myShapeDBF$PixNum
  myShapeDBF$PropPix = ifelse(myShapeDBF$PropPix <= 1, myShapeDBF$PropPix,1)
  myShapeDBF$PropPix = ifelse(is.na(myShapeDBF$PropPix),0, myShapeDBF$PropPix)
  if (is.na(match(P_HpopField,names(myShapeDBF)))){
    myHpopDB = L_CalcZonalSumRaster(G_myDepIdPath, G_myHpopPath,myShapeDBF[,P_myID])
    myShapeDBF$HpopC = myHpopDB[,2]   
  } else {
    myShapeDBF$Hpop = ifelse(myShapeDBF[,P_HpopField] == 0, NA, myShapeDBF[,P_HpopField]) 
  }
  #if (P_TypeDependent == 3){
  #myHpopDB = L_CalcZonalSum (150000, G_myDepIdPath, G_myHpopPath,myShapeDBF[,P_myID])
  #myShapeDBF$HpopC = myHpopDB[,2] 
  #}
  

#####################################################################
# Creation of the Master sampling file
#####################################################################
  print("## Creating the Master file of sampling points")
    
    myShapeDBF$Abs = ifelse(myShapeDBF[,P_myAnField] == P_myNoLiData, NaN,myShapeDBF[,P_myAnField])
    myShapeDBF$Dens = myShapeDBF$Abs/myShapeDBF$NAreaKm
    myShapeDBF$CDens = ifelse(myShapeDBF$NAreaKm > P_MinSize, myShapeDBF$Abs / (myShapeDBF$NAreaKm*myShapeDBF$Ratio), myShapeDBF$Dens)
    myShapeDBF$CDens = ifelse(is.finite(myShapeDBF$CDens),myShapeDBF$CDens,NaN)

#  if (P_TypeDependent == 3){
    if (is.na(match(P_HpopField,names(myShapeDBF)))){
    myShapeDBF$Hpop = ifelse(myShapeDBF$HpopC == 0, NA, myShapeDBF$HpopC) 
    } else {
    myShapeDBF$Hpop = ifelse(myShapeDBF[,P_HpopField] == 0, NA, myShapeDBF[,P_HpopField]) 
    }
    if (P_UseZeros == 0) {
    myShapeDBF$PCap = myShapeDBF$Abs/myShapeDBF$Hpop
    } else {
    myShapeDBF$PCap = (myShapeDBF$Abs+1)/myShapeDBF$Hpop    
    }  
#  } else {
#  myShapeDBF$PCap = 0  
#  myShapeDBF$Hpop = 0  
#  }
    

  myOutDBF = myShapeDBF[,c(P_myID,"x","y","Abs","Dens","CDens","PCap","Hpop","Ratio","Npa","PropPix","NAreaKm")]
  dir.create(paste(P_myOutData,"/",P_TempFolder,sep=""), showWarnings = FALSE)

  myOutDataFname = paste(P_myOutData,"/",P_TempFolder,"/",P_myOutName,"_Master.dbf", sep = "")
  print(paste("## Saving the Master sample file to: ",myOutDataFname))
  write.dbf(myOutDBF, myOutDataFname)

print("#############################################################")    


