##########################################################
# Create Sample files
##########################################################

# Open the dbf master file
myOutDataFname = paste(P_myOutData,"/",P_TempFolder,"/",P_myOutName,"_Master.dbf", sep = "")
myShapeDBF = read.dbf(myOutDataFname, as.is = T)
myShapeDBF$PropPix = ifelse(is.na(myShapeDBF$PropPix),0, myShapeDBF$PropPix)    
myShapeDBF$PropPixB = myShapeDBF$PropPix

#####################################################################
# Creates the XY sampling points dataframe
#####################################################################
if (P_UsePointSampling == 1){
for (nf in 1:P_nBoot){
  print("#############################################################")    
  print(paste("## Bootstrap ", nf, ": Creating sample files"))
  # Distribute the sampling points
  myTDF = L_CreateSampleRaster(G_myDepIdPath, myShapeDBF[,P_myID], myShapeDBF$PropPixB,G_mySuitMaskPath)
  # Select randomly training and test set
  myShapeDBF$IsModel = ifelse(runif(nrow(myShapeDBF))<P_myModelSetProp,1,0)
  
  #####################################################################
  # Estimate Density and corrected densities in the sample file
  #####################################################################
  # Overlay to the Id Grid File
  #myIdXYMat = as.matrix(cbind(myTDF$Id,myTDF$x,myTDF$y))
  myCoords = myTDF[c("x","y")] 
#  myTDF$IdDBF = as.vector(extract(raster(G_myDepIdPath, crs = CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")),myCoords))
  myTDF$IdDBF = myTDF$Id
  myVec = match(myTDF$IdDB,myShapeDBF[,P_myID]) 
  # Assign the Animal field to a field called "Abs"
  myTDF$Abs = myShapeDBF$Abs[myVec]
  myTDF$Dens = myShapeDBF$Dens[myVec]
  myTDF$CDens = myShapeDBF$CDens[myVec]
  myTDF$PCap = myShapeDBF$PCap[myVec]
  myTDF$Hpop = myShapeDBF$Hpop[myVec]
  myTDF$Ratio = myShapeDBF$Ratio[myVec]

  
  if (P_TypeDependent != 3){myTDF$PCap = 0}
  
  # Set as Model set or evaluation set (between polygons)
  myTDF$IsModelPg = myShapeDBF$IsModel[myVec]

  # Assign the Area field to a field called "AreaKm"
  myTDF$AreaKm = myShapeDBF$NAreaKm[myVec]
  myTDF$SAreaKm = myShapeDBF$NAreaKm[myVec]*myShapeDBF$Ratio[myVec]

  if (P_CorrectInputByMax == 1){
    myTDF$CDens = ifelse(myTDF$CDens>P_myMaxDens, P_myMaxDens,myTDF$CDens) 
    myTDF$Dens = ifelse(myTDF$Dens>P_myMaxDens, P_myMaxDens,myTDF$Dens) 
  }
  
  # Prevent negative densities to propagate in case NA codes were misinterpreted
  myTDF$Abs = ifelse(myTDF$Abs < 0, NaN,myTDF$Abs)
  myTDF$Dens = ifelse(myTDF$Dens < 0, NaN,myTDF$Dens)
  myTDF$CDens = ifelse(myTDF$CDens < 0, NaN,myTDF$CDens)
  if (P_TypeDependent == 3){myTDF$PCap = ifelse(myTDF$PCap < 0, NaN,myTDF$PCap)}

  # If zero densities should be set as NA values, do it now
  if (P_UseZeros == 0) {
    myTDF$PA = ifelse(myTDF$CDens > 0, 1,0)
    myTDF$Abs = ifelse(myTDF$Abs == 0, NaN,myTDF$Abs)
    myTDF$Dens = ifelse(myTDF$Dens == 0, NaN,myTDF$Dens)
    myTDF$CDens = ifelse(myTDF$CDens == 0, NaN,myTDF$CDens)
    if (P_TypeDependent == 3){myTDF$PCap = ifelse(myTDF$PCap == 0, NaN,myTDF$PCap)}
  }  
  
  
  # keep only the points that did intersect a shape (others had NA's) and are part of points model set
  myTDF = subset(myTDF, !is.na(Dens) & !is.na(Abs) & !is.na(CDens) & !is.na(IdDBF))
  # Adds an unique identifier to each point
  myTDF$Id = 1:nrow(myTDF)
  
  #####################################################################
  # Extract the values from the imagery
  #####################################################################
  print(paste("## Bootstrap ", nf, ": Extracting values from the imagery"))
  # Loads and convert the list of predictors
  ##########################################
  # Re-set the coordinates according to the sample file
  myCoords = myTDF[c("x","y")] 
    
  # Extract values from the list of predictors 
  ##########################################
  # Subset variables to be used for modelling
  myImTable = subset(G_myPredTable, MODEL == 1)    
  myPredVTable = subset(myImTable, TYPE == "Pred")
  for (myRec in 1:nrow(myPredVTable)){
    #myRaster = raster(myPredVTable$EXPATH[myRec], crs = CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"))
    myRaster = raster(myPredVTable$EXPATH[myRec])
    myExtraction = as.vector(extract(myRaster,myCoords))
    myTDF = cbind(myTDF,myExtraction)
    names(myTDF)[ncol(myTDF)] = myPredVTable$VARNAME[myRec]   
  }         
    
  # Extract Suitability masks & stratification 
  ##########################################
  myMaskTable = subset(myImTable, TYPE == "MskSuit" | TYPE == "MskLand" | TYPE == "MskAdmi" | TYPE == "Strat")    
  for (myRec in 1:nrow(myMaskTable)){
    #myRaster = raster(myMaskTable$EXPATH[myRec], crs = CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"))
    myRaster = raster(myMaskTable$EXPATH[myRec])
    myExtraction = as.vector(extract(myRaster,myCoords))
    myTDF = cbind(myTDF,myExtraction)
    names(myTDF)[ncol(myTDF)] = myMaskTable$VARNAME[myRec]   
  }
  
  # Remove nodata values
  ##########################################
  myTDF  = na.omit(myTDF)
  
  # Saves the dataframe
  myOutDataFname = paste(P_myOutData,"/",P_TempFolder,"/",P_myOutName,"_BootStrap_",nf,".dbf", sep = "")
  print(paste("## Bootstrap ", nf, ": Sample file in :", myOutDataFname))
  write.dbf(myTDF, myOutDataFname)
  
} # end of the loop through ExtractNsFiles
print("#############################################################")

}

if (P_UsePointSampling == 0){
    myIdRaster = raster(G_myDepIdPath)
  for (nf in 1:P_nBoot){
    print("#############################################################")    
    print(paste("## Bootstrap ", nf, ": Creating sample files"))
    # Select randomly training and test set
    myShapeDBF$IsModel = ifelse(runif(nrow(myShapeDBF))<P_myModelSetProp,1,0)
    if (P_TypeDependent != 3){myShapeDBF$PCap = 0}
    
    # Set as Model set or evaluation set (between polygons)
    myShapeDBF$IsModelPg = myShapeDBF$IsModel
    
    # Assign the Area field to a field called "AreaKm"
    myShapeDBF$AreaKm = myShapeDBF$NAreaKm
    myShapeDBF$SAreaKm = myShapeDBF$NAreaKm*myShapeDBF$Ratio
    
    if (P_CorrectInputByMax == 1){
      myShapeDBF$CDens = ifelse(myShapeDBF$CDens>P_myMaxDens, P_myMaxDens,myShapeDBF$CDens) 
      myShapeDBF$Dens = ifelse(myShapeDBF$Dens>P_myMaxDens, P_myMaxDens,myShapeDBF$Dens) 
    }
    
    # Prevent negative densities to propagate in case NA codes were misinterpreted
    myShapeDBF$Abs = ifelse(myShapeDBF$Abs < 0, NaN,myShapeDBF$Abs)
    myShapeDBF$Dens = ifelse(myShapeDBF$Dens < 0, NaN,myShapeDBF$Dens)
    myShapeDBF$CDens = ifelse(myShapeDBF$CDens < 0, NaN,myShapeDBF$CDens)
    if (P_TypeDependent == 3){myShapeDBF$PCap = myShapeDBF(myTDF$PCap < 0, NaN,myShapeDBF$PCap)}
    
    # If zero densities should be set as NA values, do it now
    if (P_UseZeros == 0) {
      myShapeDBF$PA = ifelse(myShapeDBF$CDens > 0, 1,0)
      myShapeDBF$Abs = ifelse(myShapeDBF$Abs == 0, NaN,myShapeDBF$Abs)
      myShapeDBF$Dens = ifelse(myShapeDBF$Dens == 0, NaN,myShapeDBF$Dens)
      myShapeDBF$CDens = ifelse(myShapeDBF$CDens == 0, NaN,myShapeDBF$CDens)
      if (P_TypeDependent == 3){myShapeDBF$PCap = ifelse(myShapeDBF$PCap == 0, NaN,myShapeDBF$PCap)}
    }  
    
    myShapeDBF$IdDBF = myShapeDBF[,P_myID]
    # keep only the points that did intersect a shape (others had NA's) and are part of points model set
    myTDF = subset(myShapeDBF, !is.na(Dens) & !is.na(Abs) & !is.na(CDens) & !is.na(IdDBF))
    # Adds an unique identifier to each point
    myTDF$Id = 1:nrow(myTDF)
    
    #####################################################################
    # Extract the values from the imagery
    #####################################################################
    print(paste("## Bootstrap ", nf, ": Extracting values from the imagery"))
    # Loads and convert the list of predictors
    ##########################################

    # Extract values from the list of predictors 
    ##########################################
    # Subset variables to be used for modelling
    myImTable = subset(G_myPredTable, MODEL == 1)    
    myPredVTable = subset(myImTable, TYPE == "Pred")
    for (myRec in 1:nrow(myPredVTable)){
      myRaster = raster(myPredVTable$EXPATH[myRec])
      myZonesStat = as.data.frame(zonal(myRaster,myIdRaster, fun = 'mean', na.rm = T))
      myPosVec = match(myTDF$IdDBF,myZonesStat$zone)
      myExtraction = myZonesStat$mean[myPosVec]
      myTDF = cbind(myTDF,myExtraction)
      names(myTDF)[ncol(myTDF)] = myPredVTable$VARNAME[myRec]   
    }         
    
    # Extract Suitability masks & stratification 
    ##########################################
    myMaskTable = subset(myImTable, TYPE == "MskSuit" | TYPE == "MskLand" | TYPE == "MskAdmi" | TYPE == "Strat")    
    for (myRec in 1:nrow(myMaskTable)){
      #myRaster = raster(myMaskTable$EXPATH[myRec], crs = CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"))
      myRaster = raster(myMaskTable$EXPATH[myRec])
      myZonesStat = as.data.frame(zonal(myRaster,myIdRaster, fun = 'mean', na.rm = T))
      myPosVec = match(myTDF$IdDBF,myZonesStat$zone)
      myExtraction = myZonesStat$mean[myPosVec]
      myTDF = cbind(myTDF,myExtraction)
      names(myTDF)[ncol(myTDF)] = myMaskTable$VARNAME[myRec]   
    }
    
    # Remove nodata values
    ##########################################
    myTDF  = na.omit(myTDF)
    
    # Saves the dataframe
    myOutDataFname = paste(P_myOutData,"/",P_TempFolder,"/",P_myOutName,"_BootStrap_",nf,".dbf", sep = "")
    print(paste("## Bootstrap ", nf, ": Sample file in :", myOutDataFname))
    write.dbf(myTDF, myOutDataFname)
    
  } # end of the loop through ExtractNsFiles
  print("#############################################################")
  
}




  




