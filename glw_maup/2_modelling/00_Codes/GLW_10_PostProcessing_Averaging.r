##########################################################
# Post-Processing of Model outputs
##########################################################
# Description: takes a series of predicted layers and
# calculate the mean and variability of these predictions
# 21/3/2014: changed leveling of before averaging
##########################################################
# Adjust the legend of the reported raster to make it 
# compatible with the observed

myAreaR = raster(G_myAreaPath)
myIdR = raster(G_myDepIdPath)
myMaskR = raster(G_mySuitMaskPath)
myHpopR = raster(G_myHpopPath)

myMnFilePath = paste(P_myOutData,"/",P_OutputFolder,"/",P_myOutName,"_Mn.tif", sep = "")
mySdFilePath = paste(P_myOutData,"/",P_OutputFolder,"/",P_myOutName,"_Sd.tif", sep = "")


print("######################################################")
print("## Averaging the predictions")
print("######################################################")

  myLgMaxDens = log10(P_myMaxDens+1)
  myLgMinDens = log10(P_myMinDens+1)  

  # Initialise variables for running mean
  myMask0 = myMaskR
  myMask0[myMask0 == 1] = 0
  nP = myMask0
  nMean =  myMask0
  nM2 =  myMask0

  # Loop through the bootstraps
  for (nb in 1:P_nBoot){
    myFilePath = paste(P_myOutData,"/",P_TempFolder,"/",P_myOutName,"_Nb",nb,"_RF.tif", sep = "")
    myPred = raster(myFilePath)
    if (P_PredictionAreLogged == 0) {myPred = log10(myPred+1)}

    #myPred[myPred < myLgMinDens] = myLgMinDens
    #myPred[myPred > myLgMaxDens] = myLgMaxDens
    
    # Calculations for running mean and var
    nP = nP + 1
    DeltaP = myPred - nMean
    nMean = nMean + DeltaP/nP
    nM2 = nM2 + DeltaP*(myPred-nMean)
  } # End of loops through bootstraps
  
  myMnPred = nMean
  mySdPred = (nM2 / (nP-1))^0.5
  
  myMnPred[myMnPred < myLgMinDens] = myLgMinDens
  myMnPred[myMnPred > myLgMaxDens] = myLgMaxDens
  
  # Apply suitability masking if required to
  if (P_UseSuitMask == 1) {
    myMnPred = myMnPred * myMaskR
    mySdPred = mySdPred * myMaskR 
  }
  
  # Apply administrative masking if required to
  if (P_UseAdminMask == 1) {
    myAdminMaskR = raster(G_myLandMaskPath)
    myMnPred = myMnPred * myAdminMaskR
    mySdPred = mySdPred * myAdminMaskR 
  } 
  
  writeRaster(myMnPred,myMnFilePath, overwrite = T)
  writeRaster(mySdPred,mySdFilePath, overwrite = T)
  # Set no data where no training data
  # if (P_FillGaps == 0) {
  #   myIdMaskBand = ifelse((is.na(myIdBand) | myIdBand <= 0),myNaBand,1) 
  #   MeanBand = MeanBand * myIdMaskBand
  #   SdBand = SdBand * myIdMaskBand
  # }

  # if(exists("P_PopulationCorrection")){
  #   if (P_PopulationCorrection == 1){
  #     myHpopBand = L_readIdrisiLine(G_myHpopPath,mySPixVec[x],myLengthVec[x],myHpopbit)
  #     myHpopBand = ifelse(myHpopBand == myHpopFlag, 0, myHpopBand)
  #     myHasHpopBand = ifelse(myHpopBand > 0,1,0)
  #     MeanBand = MeanBand * myHasHpopBand
  #     SdBand = SdBand * myHasHpopBand 
  #   }
  # }
  

print("######################################################")
print("## End of prediction averaging")
print("######################################################")
print(myMnFilePath)

