##########################################################
# Creates the master file with corrected densities
##########################################################
myTime = date()

print("#############################################################")    
print(paste("## Shape ID Raster conversion, started on ", myTime))
print("#############################################################")    

#####################################################################
# Open the shape file and convert it to raster with an ID by pixel
# Repeat this for the country file
#####################################################################
# Open the mask
  myMaskR = raster(G_mySuitMaskPath, native = T)
# Test if file exists
  Test = file.access(G_myDepIdPath, mode = 4)[[1]]
  if (Test == -1 | P_RedoIdRaster == 1) {
  # Open the livestock shapefile
    myShape = readShapePoly(P_myDepShape,proj4string=CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"))
  # Create the livestock ShapeID and store it in the current workspace
    myDepIdR = rasterize(myShape,myMaskR,field = P_myID)
    projection(myDepIdR) = projection(myMaskR)
    #writeGDAL(as(myDepIdR, "SpatialGridDataFrame"), fname = G_myDepIdPath, drivername = "RST", mvFlag = -1)  
    #writeRaster(myDepIdR, filename = G_myDepIdPath, format = "IDRISI", NAflag = -1, overwrite = T)
    writeRaster(myDepIdR, filename = G_myDepIdPath, overwrite = T)
    
    myTime = date()
    print(paste("## Polygon level ID raster finished at: ", myTime))
  } else {
    print("## The polygon ID raster was already present and was not re-extracted")
  }

  
  # Test = file.access(G_myCCIdPath, mode = 4)[[1]]
  # if (Test == -1 | P_RedoIdRaster == 1) {
  # Open the country shapefile
  # myShape = readShapePoly(P_myCountryShape, proj4string=CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"))
 
  
  # Create the livestock ShapeID and store it in the current workspace
  # myCCIdR = rasterize(myShape,myMaskR,field = P_myIDCC)
  #writeGDAL(as(myCCIdR, "SpatialGridDataFrame"), fname = G_myCCIdPath, drivername = "RST", mvFlag = -1)  
  # writeRaster(myCCIdR, filename = G_myCCIdPath, format = "IDRISI", NAflag = -1, overwrite = T)
  
  # myTime = date()
  #print(paste("## Country level ID raster finished at: ", myTime))
  #} else {
  #  print("## The country ID raster was already present and was not re-extracted") 

#  }
  print("#############################################################")    
print("## End of raster conversion")    
print("#############################################################")    
# Remove unused objects
#  rm(myShape)
#  rm(myMaskR)
#  rm(myDepIdR)
#  rm(myCCIdR)
#
#
#


 



