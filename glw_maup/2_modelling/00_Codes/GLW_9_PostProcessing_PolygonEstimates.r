##########################################################
# Post-Processing of Model outputs
##########################################################
# Description: takes an input prediction and correct
# by totals of an admin ID and Country ID
##########################################################
# Adjust the legend of the reported raster to make it 
# compatible with the observed

print("######################################################")
print("## Start of the estimates by polygon by bootstrap")
print("######################################################")


# Start the loop by file, bootstrap and stratification
##########################################################
for (nb in 1:P_nBoot){
  
myMnFilePath = paste(P_myOutData,"/",P_TempFolder,"/",P_myOutName,"_Nb",nb,"_RF.tif", sep = "")

Test = file.access(myMnFilePath, mode = 4)[[1]]
if (Test == -1) {
  print("######################################################")
  print(paste("## Error: The file ",myMnFilePath," was not found"))
  print("## Check the path and suffix, the current suffix is:")
  print(paste("## G_mySuffix = ", G_mySuffix))
  print("######################################################")
} else {
  
  # Loads the rasters
  myPred = raster(myMnFilePath)
  myAreaR = raster(G_myAreaPath)
  myIdR = raster(G_myDepIdPath)
  myMaskR = raster(G_mySuitMaskPath)
  myHpopR = raster(G_myHpopPath)
  
  # Estimate totals per pixel
  if (P_PredictionAreLogged == 1) {
    myPopDnR = ((10^(myPred))-1)
  } else {
    myPopDnR = myPred
    }
  myPopR = myPopDnR*myAreaR
  myHpopDnR = myHpopR / myAreaR

  # Zonalsum the area
  myPopSum = zonal(myPopR,myIdR, fun = "sum", digits = 5, na.rm = T)
  myHPopSum = zonal(myHpopR,myIdR, fun = "sum", digits = 5, na.rm = T)
  myAreaSum = zonal(myAreaR,myIdR, fun = "sum", digits = 5, na.rm = T)
  myResDF = as.data.frame(cbind(myAreaSum,myPopSum[,2],myHPopSum[,2]))
  names(myResDF) = c("Id","AreaKm","SumPop","SumHpop")

# Save the output
  write.dbf(myResDF,paste(P_myOutData,"/",P_TempFolder,"/PolById_Nb",nb,".dbf", sep = ""))

print("## End of the aggregation of polygon values")
print("######################################################")
} # end if
} # end loop

