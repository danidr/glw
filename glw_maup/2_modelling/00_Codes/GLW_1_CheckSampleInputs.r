##########################################################
# Check Inputs prior to the sampling and idrisi conversion
##########################################################
# Description: Input check
##########################################################
# By: Marius Gilbert (mgilbert@ulb.ac.be)
##########################################################
# 28th Sep. 2011
##########################################################

# Check if the shape file exists
HasShape = file.access(paste(P_myDepShape,".shp",sep = ""), mode = 4)[[1]]
HasDBF = file.access(paste(P_myDepShape,".dbf",sep = ""), mode = 4)[[1]]
if (HasDBF == 0) {
  myDBF = read.dbf(paste(P_myDepShape,".dbf",sep = ""), as.is = T)
  HasId = match(P_myID,names(myDBF))
  HasDens = match(P_myAnField,names(myDBF))
  HasArea = match(P_myArField,names(myDBF))
  # Check if area fiels
} 

#HasCShape = file.access(paste(P_myCountryShape,".shp",sep = ""), mode = 4)[[1]]
#HasCDBF = file.access(paste(P_myCountryShape,".dbf",sep = ""), mode = 4)[[1]]
#if (HasCDBF == 0) {
#  myCDBF = read.dbf(paste(P_myCountryShape,".dbf",sep = ""), as.is = T)
#  HasCId = match(P_myIDCC,names(myCDBF))
#  HasCDens = match(P_myAnCCField,names(myCDBF))
#} 



print("#####################################################")
print("## Checking sampling inputs  ")
print("#####################################################")
if (HasShape == 0) {
  print("## Info: The input shape file with animal data was found  ")
} else {
  print("## Error: The input shape file with animal data is missing on  ")
  print(paste("## ",paste(P_myDepShape,".shp",sep = "")))
}

if (HasDBF == 0) {
  print("## Info: The input dbf file with animal data was found  ")
} else {
  print("## Error: The input dbf file with animal data is missing on  ")
  print(paste("## ",paste(P_myDepShape,".dbf",sep = "")))
}

if (HasDBF == 0) {
  if (is.na(HasId)){
    print("## Error: The polygon ID field is missing  ")
  } else {
    print("## Info: The polygon ID field was found ")
    if (!is.numeric(myDBF[,P_myID])) {
      print("## Error: The polygon ID field is not numeric  ")   
    } else {
      print("## Info: The polygon ID field is numeric  ")   
    }
  }
  if (is.na(HasDens)){
    print("## Error: The animal count field is missing")
  } else {
    print("## Info: The animal count field was found ")
    if (!is.numeric(myDBF[,P_myAnField])) {
      print("## Error: The animal count field is not numeric  ")   
    } else {
      print("## Info: The animald count field is numeric  ")   
    }
    myVec = myDBF[,P_myAnField]
    myVec = ifelse(myVec == P_myNoLiData, NA, myVec)
    if (min(myVec, na.rm = T) < 0) {
      print("## Warning: Some animal count have negative values, check your inputs ")      
    }
    
  }
  if (is.na(HasArea)){
    print("## Info: The polygon area field was missing and is being estimated")
    myShape = readShapePoly(P_myDepShape,proj4string=CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"))
    myDBF$Tmp = L_areaPolygons(myShape,CRS("+proj=tcea +lat_0=65 +lon_0=10")) / 1000000
    names(myDBF)[ncol(myDBF)] = P_myArField
    write.dbf(myDBF, paste(P_myDepShape,".dbf",sep = ""))   
    rm(myShape)   
  } else {
    print("## Info: The polygon area field was found ")
    if (!is.numeric(myDBF[,P_myArField])) {
      print("## Error: The polygon area field is not numeric  ")   
    } else {
      print("## Info: The polygon area field is numeric  ")   
    }
    if (min(myDBF[,P_myArField]) <= 0) {
      print("## Error: Some areas have negative or null values, please check your inputs")      
      
      #myShape = readShapePoly(P_myDepShape,proj4string=CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"))
      #Tmp = L_areaPolygons(myShape,CRS("+proj=tcea +lat_0=65 +lon_0=10")) / 1000000
      #myDBF[,P_myArField] = ifelse(myDBF[,P_myArField] <=0,Tmp,myDBF[,P_myArField])
      #write.dbf(myDBF, paste(P_myDepShape,".dbf",sep = ""))   
      #rm(myShape)   

    }
    
  }
}

# if (HasCShape == 0) {
#   print("## Info: The country shape file with animal data has been found  ")
# } else {
#   print("## Error: The country shape file with animal data is missing on  ")
#   print(paste("## ",paste(P_myCountryShape,".shp",sep = "")))
# }
# 
# if (HasCDBF == 0) {
#   print("## Info: The country dbf file with animal data has been found  ")
# } else {
#   print("## Error: The country dbf file with animal data is missing on  ")
#   print(paste("## ",paste(P_myCountryShape,".dbf",sep = "")))
# }
# 
# if (HasCDBF == 0) {
#   
#   if (is.na(HasCId)){
#     print("## Error: The country ID field is missing  ")
#   } else {
#     print("## Info: The polygon ID field was found ")
#     if (!is.numeric(myCDBF[,P_myIDCC])) {
#       print("## Error: The polygon ID field is not numeric  ")   
#     } else {
#       print("## Info: The polygon ID field is numeric  ")   
#     }
#   }
#   if (is.na(HasCDens)){
#     print("## Error: The animal count field is missing")
#   } else {
#     print("## Info: The animal count field was found ")
#     if (!is.numeric(myCDBF[,P_myAnCCField])) {
#       print("## Error: The country animal density field is not numeric  ")   
#     } else {
#       print("## Info: The country animald density field is numeric  ")   
#     }
#     myVec = myCDBF[,P_myAnCCField]
#     myVec = ifelse(myVec == P_myNoLiData, NA, myVec)
#     if (min(myVec, na.rm = T) < 0) {
#       print("## Warning: Some country animal count have negative values, check your inputs ")      
#     }
#     
#   }
#   
# }
# 
# 

myErrorCode = 0

if (!is.numeric(P_myNoLiData)) {
  print("## Error: The no data code does not seems to be numeric, please check your inputs")
  myErrorCode = myErrorCode + 1
}

# if (!is.numeric(P_NsPol)) {
#   print("## Error: The number of points per polygon is not numeric, please check your inputs")
#   myErrorCode = myErrorCode + 1
# } else {
#   if (P_NsPol == 0) {
#     print("## Warning: The number of points per polygon is null")
#     myErrorCode = myErrorCode + 1
#   }
#   if (P_NsPol > 5) {
#     print("## Warning: The number of points per polygon is higher than 5, please check your inputs")
#     myErrorCode = myErrorCode + 1
#   }
# }


# if (!is.numeric(P_Ns)) {
#   print("## Error: The sampling point density is not numeric, please check your inputs, please check your inputs")
#   myErrorCode = myErrorCode + 1
# } else {
#   if (P_Ns < 10) {
#     print("## Warning: The sampling density is very low (< 10), please check your input, and/or proceed")
#     myErrorCode = myErrorCode + 1
#   }
#   if (P_NsPol > 100) {
#     print("## Warning: The sampling density is very high (> 100), check your inputs, and/or proceed")
#     myErrorCode = myErrorCode + 1
#   }
# }

if (myErrorCode == 0) {
  print("## Info: All numeric inputs had values in the expected range")
} else {
  print("## Warning: Some numeric inputs had unexpected values, please check them carefuly beofre proceeding")
}

print("#####################################################")
print("## End of check  ")
print("#####################################################")



# Various equal-area projections
#proj1=CRS("+proj=moll +lat_0=65 +lon_0=10") # Mollweide
#proj2=CRS("+proj=sinu +lat_0=65 +lon_0=10") # Sinusoidal
#proj3=CRS("+proj=tcea +lat_0=65 +lon_0=10") # Transverse Cylindrical
#proj4=CRS("+init=epsg:32633")               # UTM 33N (*not* equal area)

