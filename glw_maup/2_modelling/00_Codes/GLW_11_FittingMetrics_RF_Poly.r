##########################################################
# Fitting metrics
##########################################################
print("######################################################")
print("## Start of the Goodness of fit estimation script")
print("######################################################")

# Open the list of variables
##########################################################
myImTable = G_myUsedPredTable

# Loads the number of stratifications
myZVarName = "MskLand"
myNZones = 1 
myStratPath = G_myLandMaskPath



# Prepare the empty output table
##########################################################
UseUnique = 1
myPSdate = date()
myGOFTable = data.frame(temp = numeric(0))
myGOFSum = data.frame( 
  Bnum = numeric(0),
  Strat = numeric(0),
  AreaClass = numeric(0),
  Method = character(0),
  RName = character(0),
  RMSED = numeric(0),
  CORD = numeric(0),
  RMSEA = numeric(0),
  CORA = numeric(0),
  RMSEPD = numeric(0),
  CORPD = numeric(0),
  RMSEPA = numeric(0),
  CORPA = numeric(0),
  nPol = numeric(0))
myGOFSum$Method = as.character(myGOFSum$Method)
myGOFSum$RName = as.character(myGOFSum$RName)
myVarImpDB = data.frame(
  Bnum = numeric(0),
  Strat = numeric(0),
  VarName = character(0),
  IncMSE = numeric(0),
  IncNodePurity = numeric(0)
)

for (nb in 1:P_nBoot){
  # loads the sample file
  myOutDataFname = paste(P_myOutData,"/",P_TempFolder,"/",P_myOutName,"_BootStrap_",nb,".dbf", sep = "")
  mySDB = read.dbf(myOutDataFname, as.is = T) 
  mySDB = na.omit(mySDB)
  #Subset the table to the matching rows
  mySSDB = subset(mySDB, IsModelPg == 0)  
  
  # RF model prediction
  ####################################
  myRFPath = paste(P_myOutData,"/",P_TempFolder,"/",P_myOutName,"_Bootstrap_",nb,".RF", sep = "")
  load(myRFPath)
  # Makes the prediction
  myRFGen = myRF
  mySSDB$RF = predict(myRFGen, newdata = mySSDB, type="response", predict.all = FALSE)
  mySSDB$SRF = mySSDB$RF
  mySSDB$Bnum = nb
  mySSDB$RName = P_myOutName
  
  # Stores the importance of variables
  myImpM = myRF$importance
  myImpDB = as.data.frame(myImpM)
  myImpDB = cbind(rep(nb,nrow(myImpDB)),rep(0,nrow(myImpDB)),rownames(myImpM),myImpDB)
  names(myImpDB) = c("Bnum","Strat","VarName","IncMSE","IncNodePurity")
  myImpDB$VarName = as.character(myImpDB$VarName)
  myVarImpDB = rbind(myVarImpDB,myImpDB) 
  
  #Reduces the point to the unique polygons
  #########################################
  if (UseUnique == 1){
    myUniqueID = unique(mySSDB$IdDB)
    myVec = match(myUniqueID,mySSDB$IdDB)
    mySSDB = mySSDB[myVec,]
  }

  mySSDB$ObsDens = mySSDB$CDens  

  if (P_TypeDependent == 2) {mySSDB$ObsDens = mySSDB$Dens}
  if (P_TypeDependent == 3) {
    mySSDB$SNAreaKm = ifelse(mySSDB$AreaKm > P_MinSize,mySSDB$SAreaKm,mySSDB$AreaKm) 
    if (P_ApplySmallPolygonCorrection == 1) {
      mySSDB$ObsDens = (mySSDB$OPCap * mySSDB$Hpop)/mySSDB$SNAreaKm  
    } else {
      mySSDB$ObsDens = (mySSDB$PCap * mySSDB$Hpop)/mySSDB$SNAreaKm  
    }
  }

  if (P_TypeDependent == 4) {mySSDB$ObsDens = mySSDB$Abs}
  mySSDB$ObsDens = ifelse(!is.finite(mySSDB$ObsDens), NA, mySSDB$ObsDens)
  mySSDB$ObsAbs = mySSDB$ObsDens*mySSDB$SAreaKm
  
  myPolByID = read.dbf(paste(P_myOutData,"/",P_TempFolder,"/PolById_Nb",nb,".dbf", sep = ""), as.is = T)
  myPosVec = match(mySSDB$IdDBF,myPolByID$Id)
  mySSDB$SRFPolAbs = myPolByID$SumPop[myPosVec]
  mySSDB$SRFPolDens = mySSDB$SRFPolAbs/mySSDB$SAreaKm
  mySSDB$SRFPolDens = ifelse(is.finite(mySSDB$SRFPolDens),mySSDB$SRFPolDens,NA)
  
  # Adds the Zone to the table
  #myStratRaster = raster(G_myStratPath)
  #mySSDB$Zone = as.vector(extract(myStratRaster,cbind(mySSDB$x,mySSDB$y)))
  
  
  
  # Stores the GOF data
  #########################################
  myExString = c("x","y","Bnum","ObsDens","ObsAbs","Dens","CDens","PCap","Abs","Hpop","SAreaKm","RName",myZVarName,"RF","SRF","SRFPolDens")
  
  if (nrow(myGOFTable) == 0) {
    myGOFTable = mySSDB[,myExString]
    myGOFTable$RName = as.character(myGOFTable$RName)
  } else {
    myTempGOFTable = mySSDB[,myExString]
    myGOFTable = rbind(myGOFTable,myTempGOFTable)
    myGOFTable$RName = as.character(myGOFTable$RName)
  } 
  
  # Calculates GOF summary statistics
  #########################################
  # Standard density measures
  myPDF = mySSDB[,c("ObsDens","ObsAbs","SRF","SRFPolDens","SRFPolAbs",myZVarName,"SAreaKm")]
  names(myPDF)[6] = "Zone"
  myPDF$AreaClass = ifelse(myPDF$SAreaKm < 100,1,0)
  myPDF$AreaClass = ifelse(myPDF$SAreaKm >= 100 & myPDF$SAreaKm < 5000,2,myPDF$AreaClass)
  myPDF$AreaClass = ifelse(myPDF$SAreaKm >= 5000,3,myPDF$AreaClass)
  myPDF$LogObsDens = log10(myPDF$ObsDens+1)
  myPDF$LogObsAbs = log10(myPDF$ObsAbs+1)
  myPDF$LogPredDens = myPDF$SRF
  myPDF$LogPredDensPol = log10(myPDF$SRFPolDens+1)
  
  myLgMaxDens = log10(P_myMaxDens+1)
  myLgMinDens = log10(P_myMinDens+1)  
  myPDF$LogPredDens = ifelse(myPDF$LogPredDens > myLgMaxDens, myLgMaxDens, myPDF$LogPredDens)
  myPDF$LogPredDens = ifelse(myPDF$LogPredDens < myLgMinDens, myLgMinDens, myPDF$LogPredDens)
  myPDF$LogPredDensPol = ifelse(myPDF$LogPredDensPol > myLgMaxDens, myLgMaxDens, myPDF$LogPredDensPol)
  myPDF$LogPredDensPol = ifelse(myPDF$LogPredDensPol < myLgMinDens, myLgMinDens, myPDF$LogPredDensPol)
  myPDF$LogPredAbs = log10(((10^(myPDF$LogPredDens)-1)*myPDF$SAreaKm)+1)
  myPDF$LogPredAbsPol = log10(((10^(myPDF$LogPredDensPol)-1)*myPDF$SAreaKm)+1)

#   RMSE_Dens = numeric(0),
#  COR_Dens = numeric(0),
#   RMSE_Abs = numeric(0),
#   COR_Abs = numeric(0),
#   RMSE_PolDens = numeric(0),
#   COR_PolDens = numeric(0),
#   RMSE_PolAbs = numeric(0),
#   COR_PolAbs = numeric(0),  
  
  
  myPDF = subset(myPDF, LogObsDens > 0 & LogPredDens > 0)
  myPDF = na.omit(myPDF)
  #Pt
  myCOR_Dens = cor(myPDF$LogObsDens, myPDF$LogPredDens)
  myRMSE_Dens = (mean((myPDF$LogObsDens - myPDF$LogPredDens)^2)^0.5)
  myCOR_Abs = cor(myPDF$LogObsAbs, myPDF$LogPredAbs)
  myRMSE_Abs = (mean((myPDF$LogObsAbs - myPDF$LogPredAbs)^2)^0.5)
  #Pol
  myCOR_PolDens = cor(myPDF$LogObsDens, myPDF$LogPredDensPol)
  myRMSE_PolDens = (mean((myPDF$LogObsDens - myPDF$LogPredDensPol)^2)^0.5)
  myCOR_PolAbs = cor(myPDF$LogObsAbs, myPDF$LogPredAbsPol)
  myRMSE_PolAbs = (mean((myPDF$LogObsAbs - myPDF$LogPredAbsPol)^2)^0.5)
  
  myRow = list(nb,0,0,"RF",P_myOutName,myRMSE_Dens,myCOR_Dens,myRMSE_Abs,myCOR_Abs,myRMSE_PolDens,myCOR_PolDens,myRMSE_PolAbs,myCOR_PolAbs, nrow(myPDF))
  names(myRow) = c("Bnum","Strat","AreaClass","Method","RName","RMSED","CORD","RMSEA","CORA","RMSEPD","CORPD","RMSEPA","CORPA","nPol")
  myGOFSum = rbind(myGOFSum,myRow)
  
  
  for (ac in 1:3) {
      myPDFS = subset(myPDF, AreaClass == ac)
      #Pts
      myCOR_Dens = cor(myPDFS$LogObsDens, myPDFS$LogPredDens)
      myRMSE_Dens = (mean((myPDFS$LogObsDens - myPDFS$LogPredDens)^2)^0.5)
      myCOR_Abs = cor(myPDFS$LogObsAbs, myPDFS$LogPredAbs)
      myRMSE_Abs = (mean((myPDFS$LogObsAbs - myPDFS$LogPredAbs)^2)^0.5)
      #Pol
      myCOR_PolDens = cor(myPDFS$LogObsDens, myPDFS$LogPredDensPol)
      myRMSE_PolDens = (mean((myPDFS$LogObsDens - myPDFS$LogPredDensPol)^2)^0.5)
      myCOR_PolAbs = cor(myPDFS$LogObsAbs, myPDFS$LogPredAbsPol)
      myRMSE_PolAbs = (mean((myPDFS$LogObsAbs - myPDFS$LogPredAbsPol)^2)^0.5)

      myRow = list(nb,0,ac,"RF",P_myOutName,myRMSE_Dens,myCOR_Dens,myRMSE_Abs,myCOR_Abs,myRMSE_PolDens,myCOR_PolDens,myRMSE_PolAbs,myCOR_PolAbs, nrow(myPDFS))
      names(myRow) = c("Bnum","Strat","AreaClass","Method","RName","RMSED","CORD","RMSEA","CORA","RMSEPD","CORPD","RMSEPA","CORPA","nPol")
      myGOFSum = rbind(myGOFSum,myRow)
   }
  

} # End of loops through bootstraps

myGOFSum = na.omit(myGOFSum)
myGOFSum$Method = as.character(myGOFSum$Method)
myGOFSum$RName = as.character(myGOFSum$RName)

# Saves the Output GOF metrics
########################################
myGOFTablePath = paste(P_myOutData,"/",P_OutputFolder,"/",P_myOutName,"_GOFData.dbf", sep = "")
write.dbf(myGOFTable, myGOFTablePath)
myGOFSumPath = paste(P_myOutData,"/",P_OutputFolder,"/",P_myOutName,"_GOFSummary.dbf", sep = "")
write.dbf(myGOFSum, myGOFSumPath)
myVarDBPath = paste(P_myOutData,"/",P_OutputFolder,"/",P_myOutName,"_VarImp.dbf", sep = "")
write.dbf(myVarImpDB, myVarDBPath)

print("## GOF metrics estimated and saved")
print("######################################################")




