##########################################################
# Libraries
##########################################################
# Description: Functions used by the other scripts
##########################################################
# By: Marius Gilbert (mgilbert@ulb.ac.be)
# Last updates Oct 26 2017 to make more use of raster package
##########################################################

##########################################################
## 1 Loads required packages
##########################################################
#.libPaths(c('/export/data/ilri/glw/0_Program/Rlibs',.libPaths()))
library(maptools)
library(foreign)
library(raster)
library(rgdal)
library(RColorBrewer) #
library(classInt)
#library(gbm)
#library(dismo)
library(randomForest)
library(knitr)
library(rmarkdown)
library(xtable)
library(pander)
setwd(P_GenPath)
opts_knit$set(root.dir = P_GenPath)


#########################################################
## Write KML
##########################################################
# write.KML = function(myFunShape,myFunField,myFunPath,myHeader,myDesc,myColor){
#   out <- sapply(slot(myFunShape, "polygons"), function(x) {
#     kmlPolygon(x,
#                name=as(myFunShape, "data.frame")[slot(x, "ID"), myFunField],     
#                #col="red", 
#                lwd=1.5, border= myColor, 
#                description=paste("SHAPEID:", slot(x, "ID")))
#   }
#   )  # outside of 'sapply' function call
#   kmlFile <- file(myFunPath, "w")  
#   cat(kmlPolygon(kmlname=myHeader, kmldescription=myDesc)$header,file=kmlFile, sep="\n")
#   cat(unlist(out["style",]), file=kmlFile, sep="\n")
#   cat(unlist(out["content",]), file=kmlFile, sep="\n")
#   cat(kmlPolygon()$footer, file=kmlFile, sep="\n")
#   close(kmlFile)  
# }
# 

##########################################################
## L_ZonalSum
##########################################################
## Description: Calculate the sum of an external layer
##########################################################
# Last updates: January 2014
# By: Marius Gilbert (mgilbert@ulb.ac.be)
##########################################################
#Isused = Y
L_CalcZonalSum = function (BandSize, myIdPath, myVarPath,myIdInVec){
    #BandSize = 150000
    #myIdPath = G_myDepIdPath
    #myVarPath = P_HpopPath
    #myIdInVec = myShapeDBF[,P_myID]
    
    #####################################################################
    # Find the dimension of the input files and estimates the size of the
    # bands
    #####################################################################
      myDims = L_GetLoopDimensions(BandSize,myIdPath)
      Nband = myDims[[1]]
      mySPixVec = myDims[[2]]
      myLengthVec = myDims[[3]]
    
    #####################################################################
    # Create a dataframe to store Suitable area
    #####################################################################
    myDFSum = data.frame(cbind(vector(mode = "numeric",0),vector(mode = "numeric",0)))
    names(myDFSum) = c("Id","Sum")
    
    #####################################################################
    # Loops through the bands
    #####################################################################
    myIdbit = L_GetHeader(myIdPath)[4]
    myVarbit = L_GetHeader(myVarPath)[4]

    for (x in 1:Nband) {
      # Get the three bands (Idcode, Suitability, SurfaceArea)
        myIdBand = L_readIdrisiLine(myIdPath,mySPixVec[x],myLengthVec[x],myIdbit)
        myVarBand = L_readIdrisiLine(myVarPath,mySPixVec[x],myLengthVec[x],myVarbit)
        # Replaces negative values by na
        myVarBand = ifelse(myVarBand<0,NA,myVarBand)
        # Replaces no data values by zero in the mask
        myVarBand = ifelse(is.na(myVarBand),0,myVarBand)
      
      # Summarise Suitable area by unique IdCode
        myTmpDF = as.data.frame(cbind(myIdBand, myVarBand))
        myResDF = aggregate(myTmpDF$myVarBand, list(Id = myTmpDF$myIdBand), sum)
        names(myResDF) = c("Id","Sum")
      # Adds the result to the Previous Idcode found in the Table
      myDFSum = rbind(myDFSum, myResDF) 
    }
    
    #####################################################################
    # End of the loop through the bands
    #####################################################################
    
    #####################################################################
    # Post Processing and returning the output columns
    #####################################################################
    # Aggregate the values by Unique Code
      myResDF = aggregate(myDFSum$Sum, list(Id = myDFSum$Id), sum)
      names(myResDF) = c("Id","Sum")
        
      myV = match(myIdInVec,myResDF$Id)
      myId = myIdInVec
      mySum = myResDF$Sum[myV]
  
      myOutput = cbind(myId,mySum)
    return(myOutput)
}




##########################################################
## L_CalcSuitArea
##########################################################
## Description: Calculate suitable areas and returns a three 
## column DF with the respective Areas, suitable Area, and Ratio
##########################################################
# Last updates: 3rd May 2011
# By: Marius Gilbert (mgilbert@ulb.ac.be)
##########################################################
#Isused = Y
L_CalcSuitArea = function (BandSize, myMaskPath, myIdPath, myAreaPath,myIdInVec){
    #BandSize = 150000
    #G_mySuitMaskPath, G_myDepIdPath, G_myAreaPath,myShapeDBF[,P_myID]
    #myMaskPath = G_mySuitMaskPath
    #myIdPath = G_myDepIdPath
    #myAreaPath = G_myAreaPath
    #myIdInVec = myShapeDBF[,P_myID]
    
    #####################################################################
    # Find the dimension of the input files and estimates the size of the
    # bands
    #####################################################################
      myDims = L_GetLoopDimensions(BandSize,myMaskPath)
      Nband = myDims[[1]]
      mySPixVec = myDims[[2]]
      myLengthVec = myDims[[3]]
    
    #####################################################################
    # Create a dataframe to store Suitable area
    #####################################################################
    myDFSuit = data.frame(cbind(vector(mode = "numeric",0),vector(mode = "numeric",0),vector(mode = "numeric",0)))
    names(myDFSuit) = c("Id","Area","SuitArea")
    
    #####################################################################
    # Loops through the bands
    #####################################################################
    myIdbit = L_GetHeader(myIdPath)[4]
    myMaskbit = L_GetHeader(myMaskPath)[4]
    myAreabit = L_GetHeader(myAreaPath)[4]
    for (x in 1:Nband) {
      # Get the three bands (Idcode, Suitability, SurfaceArea)
        myIdBand = L_readIdrisiLine(myIdPath,mySPixVec[x],myLengthVec[x],myIdbit)
        myMskBand = L_readIdrisiLine(myMaskPath,mySPixVec[x],myLengthVec[x],myMaskbit)
        myAreaBand = L_readIdrisiLine(myAreaPath,mySPixVec[x],myLengthVec[x],myAreabit)
        myAreaBand = myAreaBand / P_myAreaFactor     
      # Replaces no data values by zero in the mask
        myMskBand = ifelse(is.na(myMskBand),0,myMskBand)
        myMskBand = ifelse(myMskBand<0,0,myMskBand)
      # Calculate a band with suitable area
        mySuitAreaBand = myMskBand * myAreaBand
      # Create a band for pixel number
        myPixBand = (mySuitAreaBand*0)+1
      # Summarise Suitable area by unique IdCode
        myTmpDF = as.data.frame(cbind(myIdBand, myAreaBand,mySuitAreaBand,myPixBand))
        myTmpAgrArea = aggregate(myTmpDF$myAreaBand, list(Id = myTmpDF$myIdBand), sum)
        myTmpAgrSuitArea = aggregate(myTmpDF$mySuitAreaBand, list(Id = myTmpDF$myIdBand), sum)
        myTmpPix = aggregate(myTmpDF$myPixBand, list(Id = myTmpDF$myIdBand), sum)
        myResDF = cbind(myTmpAgrArea,myTmpAgrSuitArea$x,myTmpPix$x)
        names(myResDF) = c("Id","Area","SuitArea","NumPix")
      # Adds the result to the Previous Idcode found in the Table
      myDFSuit = rbind(myDFSuit, myResDF) 
    }
    
    #####################################################################
    # End of the loop through the bands
    #####################################################################
    
    #####################################################################
    # Post Processing and returning the output columns
    #####################################################################
    # Aggregate the values by Unique Code
      myTmpAgrArea = aggregate(myDFSuit$Area, list(Id = myDFSuit$Id), sum)
      myTmpAgrSuitArea = aggregate(myDFSuit$SuitArea, list(Id = myDFSuit$Id), sum)
      myTmpAgrPix = aggregate(myDFSuit$NumPix, list(Id = myDFSuit$Id), sum)
    
      myResDF = cbind(myTmpAgrArea,myTmpAgrSuitArea$x,myTmpAgrPix$x )
      names(myResDF) = c("Id","AreaKm","SuitAreaKm","NumPix")
      myResDF$Ratio = ifelse(myResDF$AreaKm > 0, myResDF$SuitAreaKm / myResDF$AreaKm,1)
    
      myV = match(myIdInVec,myResDF$Id)
      myAreaKmC = myResDF$AreaKm[myV]
      mySAreaKmC = myResDF$SuitAreaKm[myV]
      myRatio = myResDF$Ratio[myV]
      myPixNum = myResDF$NumPix[myV]
      myOutput = cbind(myAreaKmC,mySAreaKmC,myRatio,myPixNum)
    return(myOutput)
}

##########################################################
## L_CalcSuitAreaRaster
##########################################################
## Description: Calculate suitable areas and returns a three 
## column DF with the respective Areas, suitable Area, and Ratio
## But using raster zonation functions
## the functions writes temporary files and needs to be
## checked over large grids
##########################################################
# Last updates: 3rd May 2011
# By: Marius Gilbert (mgilbert@ulb.ac.be)
##########################################################
#Isused = Y
L_CalcSuitAreaRaster = function (myMaskPath, myIdPath, myAreaPath,myIdInVec){
  #BandSize = 150000
  #G_mySuitMaskPath, G_myDepIdPath, G_myAreaPath,myShapeDBF[,P_myID]
  #myMaskPath = G_mySuitMaskPath
  #myIdPath = G_myDepIdPath
  #myAreaPath = G_myAreaPath
  #myIdInVec = myShapeDBF[,P_myID]
  #myIdPath = G_myDepIdPath
  #myIdInVec = myShapeDBF[,P_myID]
  #myAreaPath = G_myAreaPath
  #myMaskPath = G_mySuitMaskPath
  
  # Loads the raster
  myIdR = raster(myIdPath)
  myMaskR = raster(myMaskPath)
  myAreaR = raster(myAreaPath)
  # Zonalsum the area
  myAreaSum = zonal(myAreaR,myIdR, fun = "sum", digits = 5, na.rm = T)
  # Estimate the suitability land area and zonal sum it
  mySAreaR = myMaskR * myAreaR
  mySAreaSum = zonal(mySAreaR,myIdR, fun = "sum", digits = 5, na.rm = T)
  # Create a layer with a value of 1 by pixel and sumarize to have pixel counts
  myPixelR = (myMaskR*0)+1
  myPixelSum = zonal(myPixelR,myIdR, fun = "sum", digits = 5, na.rm = T)
  
  # Summarize the results and create the output layer
  myResDF = as.data.frame(cbind(myAreaSum[,1], myAreaSum[,2],mySAreaSum[,2],myPixelSum[,2]))
  names(myResDF) = c("Id","AreaKm","SuitAreaKm","NumPix")
  myResDF$Ratio = ifelse(myResDF$AreaKm > 0, myResDF$SuitAreaKm / myResDF$AreaKm,1)
  myV = match(myIdInVec,myResDF$Id)
  myAreaKmC = myResDF$AreaKm[myV]
  mySAreaKmC = myResDF$SuitAreaKm[myV]
  myRatio = myResDF$Ratio[myV]
  myPixNum = myResDF$NumPix[myV]
  myOutput = cbind(myAreaKmC,mySAreaKmC,myRatio,myPixNum)
  return(myOutput)
}

L_CalcZonalSumRaster = function (myIdPath, mySumPath,myIdInVec){
  #myIdPath = G_myDepIdPath
  #mySumPath = G_myHpopPath
  #myIdInVec = myShapeDBF[,P_myID]

  # Loads the raster
  myIdR = raster(myIdPath)
  mySumR = raster(mySumPath)
  # Zonalsum the area
  myAreaSum = zonal(mySumR,myIdR, fun = "sum", digits = 5, na.rm = T)

  # Summarize the results and create the output vectors
  myResDF = as.data.frame(cbind(myAreaSum[,1], myAreaSum[,2]))
  names(myResDF) = c("Id","Sum")
  myV = match(myIdInVec,myResDF$Id)
  myId = myIdInVec
  mySum = myResDF$Sum[myV]
  myOutput = cbind(myId,mySum)
  
  return(myOutput)
}



##########################################################
## L_GetLoopDimensions
##########################################################
## Description: Get the dimensions of bands for band
## by band treatement of data
##########################################################
# Last updates: 3rd May 2011
# By: Marius Gilbert (mgilbert@ulb.ac.be)
##########################################################
#Isused = Y
L_GetLoopDimensions = function(BandSize, ImagePath){
  # Get the total size of images in number of pixels
  myLength = L_GetHeader(ImagePath)[3]
  # Estimate the number of bands and the size of the last band
  Nband = trunc(myLength/BandSize)
  # Estimates the residuals size
  BandRes = myLength - (Nband * BandSize)
  # Create vector with reading position and length
  # of each band
  mySPixVec = seq(1, (Nband * BandSize + 1), BandSize)
  myLengthVec = (mySPixVec * 0) + BandSize
  myLengthVec[length(myLengthVec)] = BandRes
  if(BandRes > 0) {Nband = Nband + 1}
  myOutputs = list(Nband,mySPixVec,myLengthVec)
  return(myOutputs)
}


##########################################################
## L_CreateSample
##########################################################
## Description: Create sample points distributed with
# an equal density per km2
##########################################################
# Last updates: 3rd May 2011
# By: Marius Gilbert (mgilbert@ulb.ac.be)
##########################################################
#IsUsed = Y
L_CreateSample = function (BandSize, myIdPath, myIdInVec,myPropPix,myMaskPath){
  #BandSize = 150000
  #myIdPath = G_myDepIdPath
  #myIdInVec = myShapeDBF[,P_myID]
  #myPropPix = myShapeDBF$PropPix
  #myMaskPath = G_mySuitMaskPath
  
  # Find the dimension of the input files and estimates the size of the
  # bands
  #####################################################################
  myDims = L_GetLoopDimensions(BandSize,myMaskPath)
  Nband = myDims[[1]]
  mySPixVec = myDims[[2]]
  myLengthVec = myDims[[3]]
  
  #####################################################################
  # Summarize by uniqueID and estimate the number of sampling point ratio
  #####################################################################
  myAgrPixels = data.frame(cbind(myIdInVec,myPropPix))
  names(myAgrPixels) = c("Id","Prop")
  
  #####################################################################
  # Loop over the bands to select the sampling points
  #####################################################################
  myIdbit = L_GetHeader(myIdPath)[4]
  myDFSample = data.frame(cbind(vector(mode = "numeric",0),vector(mode = "numeric",0),vector(mode = "numeric",0)))
  names(myDFSample) = c("IsSampleBand","myNumPixBand","myIdBand")
  
  for (x in 1:Nband) {
    # Get the three bands (Idcode, Suitability, SurfaceArea)
    myIdBand = L_readIdrisiLine(myIdPath,mySPixVec[x],myLengthVec[x],myIdbit)
    myV = match(myIdBand,myAgrPixels$Id)
    myPropBand = myAgrPixels$Prop[myV]
    IsSampleBand = ifelse(is.na(myPropBand),0,ifelse(runif(myLengthVec[x])<myPropBand,1,0))
    myNumPixBand = mySPixVec[x] + ((1:myLengthVec[x])-1)
    myBDDF = as.data.frame(cbind(IsSampleBand,myNumPixBand,myIdBand))
    myBDDF = subset(myBDDF, IsSampleBand == 1)
    myDFSample = rbind(myDFSample, myBDDF)
  }
  
  #####################################################################
  # Re-estimate the XY coordinates of those sampling points
  #####################################################################
  myFile = myIdPath
  ImType = (strsplit(myFile,"\\."))[[1]][2]
  ImName = (strsplit(myFile,"\\."))[[1]][1]
  ext = ".rdc"
  MyfileRDC = paste (ImName,ext,sep ="")
  MyfileRDC = as.character (MyfileRDC)
  DocList = scan(MyfileRDC,what = list("",""), nmax = 22, sep = ":", quote = "", strip.white = TRUE, skip = 2, nlines = 22, quiet = T)
  DocPath = (cbind(DocList[[1]],DocList[[2]]))
  theNtype = DocPath[match("data type",DocPath[,1]),2]
  theNCols = as.integer(DocPath[match("columns",DocPath[,1]),2])
  theNRows = as.integer(DocPath[match("rows",DocPath[,1]),2])
  theLLXCoord = as.numeric(DocPath[match("min. X",DocPath[,1]),2])
  theURXCoord = as.numeric(DocPath[match("max. X",DocPath[,1]),2])
  theLLYCoord = as.numeric(DocPath[match("min. Y",DocPath[,1]),2])
  theURYCoord = as.numeric(DocPath[match("max. Y",DocPath[,1]),2])
  myFlagValue = DocPath[match("flag value",DocPath[,1]),2]
  MyNoValuecode = ifelse(myFlagValue == "none", -9999, as.numeric(myFlagValue))
  theXSize = (theURXCoord - theLLXCoord) / theNCols
  theYSize = (theURYCoord - theLLYCoord) / theNRows
  LF =  theNCols * theNRows
  
  theYPos = trunc(myDFSample$myNumPixBand / theNCols)+1
  theXPos = myDFSample$myNumPixBand  - ((theYPos-1) * theNCols)
  myDFSample$x = (theXPos * theXSize) + theLLXCoord
  myDFSample$y = theURYCoord - (theYPos * theYSize)
  myRetDF = myDFSample[c("x","y","myIdBand")]
  names(myRetDF) = c("x","y","IdDB")
  return(myRetDF)
}

L_CreateSampleRaster = function (myIdPath, myIdInVec,myPropPix,myMaskPath){
  #myIdPath = G_myDepIdPath
  #myIdInVec = myShapeDBF[,P_myID]
  #myPropPix = myShapeDBF$PropPix
  #myMaskPath = G_mySuitMaskPath
  
  # Reclassifies the idraster into proportion of sampling points
  myIdR = raster(myIdPath)
  myMaskR = raster(myMaskPath)
  myPropPosR = reclassify(myIdR, cbind(myIdInVec,myPropPix))
  myPropPosR = myPropPosR**(myMaskR>=0)
  
  # Draws random numbers at the pixel level
  myRunR = setValues(myIdR,runif(ncell(myMaskR)))*(myMaskR>=0)
  myPosPixels = myRunR < myPropPosR
  sum(values(myPosPixels),na.rm = T)
  # Convert the positives raster into a sample location
  myPts  = rasterToPoints(myPosPixels, function(x) x == 1)
  x = myPts[,1]
  y = myPts[,2]
  Id = extract(myIdR, cbind(x,y))
  myRetDF = as.data.frame(cbind(x,y,Id))
  names(myRetDF) = c("x","y","IdDB")
  return(myRetDF)
}


##########################################################
## L_GetHeader
##########################################################
## Description: Get header information from an Idrisi
##              *.rdc file
##########################################################
# Last updates: 5th November 2009
# By: Marius Gilbert (mgilbert@ulb.ac.be)
##########################################################
## Arguments:
## ImPath: Path to the *.rst file
##########################################################
# Isused = true

L_GetHeader = function(ImPath){
  ImType = (strsplit(ImPath,"\\."))[[1]][2]
  ImName = (strsplit(ImPath,"\\."))[[1]][1]
  ext = ".rdc"
  MyfileRDC = as.character(paste (ImName,ext,sep =""))
  DocList = scan(MyfileRDC,what = list("",""), nmax = 22, sep = ":", quote = "", strip.white = TRUE, skip = 2, nlines = 22, quiet = T)
  DocPath = (cbind(DocList[[1]],DocList[[2]]))
  theNtype = DocPath[match("data type",DocPath[,1]),2]
  theNCols = as.integer(DocPath[match("columns",DocPath[,1]),2])
  theNRows = as.integer(DocPath[match("rows",DocPath[,1]),2])
  theLLXCoord = as.numeric(DocPath[match("min. X",DocPath[,1]),2])
  theURXCoord = as.numeric(DocPath[match("max. X",DocPath[,1]),2])
  theLLYCoord = as.numeric(DocPath[match("min. Y",DocPath[,1]),2])
  theURYCoord = as.numeric(DocPath[match("max. Y",DocPath[,1]),2])
  theXSize = (theURXCoord - theLLXCoord) / theNCols
  theYSize = (theURYCoord - theLLYCoord) / theNRows
  LF =  theNCols * theNRows
  #TheTypeValue = as.numeric (TheTypeValue)
  if (theNtype == "integer" ){bit = 2}
  if (theNtype == "byte" ){bit = 1}
  if (theNtype == "real" ){bit = 4}
  return(c(theNCols,theNRows, LF, bit,theLLXCoord, theLLYCoord,theXSize,theURXCoord, theURYCoord))
}

L_GetFlagValue = function(ImPath){
  ImType = (strsplit(ImPath,"\\."))[[1]][2]
  ImName = (strsplit(ImPath,"\\."))[[1]][1]
  ext = ".rdc"
  MyfileRDC = as.character(paste (ImName,ext,sep =""))
  DocList = scan(MyfileRDC,what = list("",""), nmax = 22, sep = ":", quote = "", strip.white = TRUE, skip = 2, nlines = 22, quiet = T)
  DocPath = (cbind(DocList[[1]],DocList[[2]]))
  theFlag = DocPath[match("flag value",DocPath[,1]),2]
  if (theFlag == "none" | theFlag == "unknown"){
    theFlag = -9999  
  } else{
    theFlag = as.numeric(theFlag)  
  }
  return(theFlag)
}

##########################################################
## L_readIdrisiLine
##########################################################
## Description: Reads a band from an Idrisi image *.rst
##########################################################
# Last updates: 5th November 2009
# By: Marius Gilbert (mgilbert@ulb.ac.be)
##########################################################
## Arguments:
## myFile: Path to the *.rst file
## mySId : Position of the starting pixel to extract
## Nrec: number of pixels to extract
## bit: number of bytes per pixel (1,2 or 4)
##########################################################
## Test parameters
## mySId = 1
## Nrec = 100
## myFile = "C:/PouMapCh/Data/Predictors/Test.RST"
##########################################################
#Isused = Y
L_readIdrisiLine = function (myFile, mySId, Nrec, bit){
  MyCon <- file(myFile, "rb")
  myPos = (mySId - 1) * bit
  seek(MyCon, myPos, origin = "start", rw = "read")
  if  (bit == 1) {myVec = readBin(MyCon, integer(), n = Nrec, size = 1, signed = F)}
  if  (bit == 2) {myVec = readBin(MyCon, integer(), n = Nrec, size = 2)}
  if  (bit == 4) {myVec = readBin(MyCon, numeric(), n = Nrec, size = 4)}
  close(MyCon)
  return(myVec)
}
L_readIdrisiLineN = function (myFile, mySId, Nrec){
  bit = L_GetHeader(myFile)[[4]]
  myFlag = L_GetFlagValue(myFile)
  MyCon <- file(myFile, "rb")
  myPos = (mySId - 1) * bit
  seek(MyCon, myPos, origin = "start", rw = "read")
  if  (bit == 1) {myVec = readBin(MyCon, integer(), n = Nrec, size = 1, signed = F)}
  if  (bit == 2) {myVec = readBin(MyCon, integer(), n = Nrec, size = 2)}
  if  (bit == 4) {myVec = readBin(MyCon, numeric(), n = Nrec, size = 4)}
  close(MyCon)
  myNaBand = rep(NaN,length(myVec))
  myVec = ifelse(myVec == myFlag,myNaBand,myVec) 
  return(myVec)
}


##########################################################
## L_AssignModelSet
##########################################################
## Description: Assigns data points to model or evaluation
## set based on a given proportion of unique ID
##########################################################
# Last updates: 3rd May 2011
# By: Marius Gilbert (mgilbert@ulb.ac.be)
##########################################################
#IsUsed = Y
L_AssignModelSet = function (myDF, myModelSetProp, IDField){
  #IDField = "IdDB"
  myUniqueID = unique(myDF[,IDField])
  NsModel = trunc(myModelSetProp * length(myUniqueID))
  myModelID = sample(myUniqueID,NsModel, replace = T)
  myVec = !is.na(match(myDF[,IDField],myModelID))
  myReturnVec = ifelse(myVec,1,0)
  return(myReturnVec)
}


##########################################################
## L_WriteRDC
##########################################################
## Description: Write the rdc file of an image
##########################################################
# Last updates: 3rd May 2011
# By: Marius Gilbert (mgilbert@ulb.ac.be)
##########################################################
#IsUsed = T
L_WriteRDC = function(myRSTFilePath,myHeaderPath,myTitle,myMax,myMin,myType){
  ImName = (strsplit(myRSTFilePath,"\\."))[[1]][1]
  myOutRDCPath = as.character(paste(ImName,".rdc",sep =""))
  myHeaders = L_GetHeader(myHeaderPath)
  # Open the connexion
  MyOutCon <- file(myOutRDCPath, "w")
  write("file format : IDRISI Raster A.1", MyOutCon, append = T)
  write(paste("file title  : ",myTitle, sep = ""), MyOutCon, append = T)
  write(paste("data type   : ",myType, sep = ""), MyOutCon, append = T)
  write("file type   : binary", MyOutCon, append = T)
  write(paste("columns     : ",myHeaders[1],sep = ""), MyOutCon, append = T)
  write(paste("rows        : ",myHeaders[2],sep = ""), MyOutCon, append = T)
  write("ref. system : latlong", MyOutCon, append = T)
  write("ref. units  : deg", MyOutCon, append = T)
  write("unit dist.  : 1.0000000", MyOutCon, append = T)
  write(paste("min. X      : ",myHeaders[5],sep = ""), MyOutCon, append = T)
  write(paste("max. X      : ",myHeaders[8],sep = ""), MyOutCon, append = T)
  write(paste("min. Y      : ",myHeaders[6],sep = ""), MyOutCon, append = T)
  write(paste("max. Y      : ",myHeaders[9],sep = ""), MyOutCon, append = T)
  write("pos'n error : unknown", MyOutCon, append = T)
  write(paste("resolution  : ",myHeaders[7],sep = ""), MyOutCon, append = T)
  write(paste("min. value  : ",myMin,sep = ""), MyOutCon, append = T)
  write(paste("max. value  : ",myMax,sep = ""), MyOutCon, append = T)
  write(paste("display min : ",myMin,sep = ""), MyOutCon, append = T)
  write(paste("display max : ",myMax,sep = ""), MyOutCon, append = T)
  write("value units : unspecified", MyOutCon, append = T)
  write("value error : unknown", MyOutCon, append = T)
  write("flag value  : -9999", MyOutCon, append = T)
  write("flag def'n  : no data", MyOutCon, append = T)
  write("legend cats : 0", MyOutCon, append = T)
  write("lineage     : ", MyOutCon, append = T)
  write("lineage     : ", MyOutCon, append = T)
  # Close the connexion
  close(MyOutCon)
}

L_WriteRDCF = function(myRSTFilePath,myHeaderPath,myTitle,myMax,myMin,myType,myFlag){
  ImName = (strsplit(myRSTFilePath,"\\."))[[1]][1]
  myOutRDCPath = as.character(paste(ImName,".rdc",sep =""))
  myHeaders = L_GetHeader(myHeaderPath)
  # Open the connexion
  MyOutCon <- file(myOutRDCPath, "w")
  write("file format : IDRISI Raster A.1", MyOutCon, append = T)
  write(paste("file title  : ",myTitle, sep = ""), MyOutCon, append = T)
  write(paste("data type   : ",myType, sep = ""), MyOutCon, append = T)
  write("file type   : binary", MyOutCon, append = T)
  write(paste("columns     : ",myHeaders[1],sep = ""), MyOutCon, append = T)
  write(paste("rows        : ",myHeaders[2],sep = ""), MyOutCon, append = T)
  write("ref. system : latlong", MyOutCon, append = T)
  write("ref. units  : deg", MyOutCon, append = T)
  write("unit dist.  : 1.0000000", MyOutCon, append = T)
  write(paste("min. X      : ",myHeaders[5],sep = ""), MyOutCon, append = T)
  write(paste("max. X      : ",myHeaders[8],sep = ""), MyOutCon, append = T)
  write(paste("min. Y      : ",myHeaders[6],sep = ""), MyOutCon, append = T)
  write(paste("max. Y      : ",myHeaders[9],sep = ""), MyOutCon, append = T)
  write("pos'n error : unknown", MyOutCon, append = T)
  write(paste("resolution  : ",myHeaders[7],sep = ""), MyOutCon, append = T)
  write(paste("min. value  : ",myMin,sep = ""), MyOutCon, append = T)
  write(paste("max. value  : ",myMax,sep = ""), MyOutCon, append = T)
  write(paste("display min : ",myMin,sep = ""), MyOutCon, append = T)
  write(paste("display max : ",myMax,sep = ""), MyOutCon, append = T)
  write("value units : unspecified", MyOutCon, append = T)
  write("value error : unknown", MyOutCon, append = T)
  write(paste("flag value  : ",myFlag,sep = ""), MyOutCon, append = T)
  write("flag def'n  : no data", MyOutCon, append = T)
  write("legend cats : 0", MyOutCon, append = T)
  write("lineage     : ", MyOutCon, append = T)
  write("lineage     : ", MyOutCon, append = T)
  # Close the connexion
  close(MyOutCon)
}

# Function used to estimate the area of each polygone
#####################################################################
# Programmer: Dan Putler
# Created: 06Nov09
# Modified: 06Nov09
#####################################################################
L_areaPolygons<- function(spPoly, proj4string = NULL) {
  if(class(spPoly)[[1]] != "SpatialPolygonsDataFrame"& class(spPoly)[[1]] != "SpatialPolygons") {
    stop("spPoly must be a SpatialPolygonsDataFrame or a SpatialPolygons object.")
  }
  require(sp)
  require(rgdal)
  if(!is.null(proj4string)) {
    if(class(proj4string)[[1]] != "CRS") {
      stop("The proj4string must be of class CRS")
    }
    spP<- spTransform(spPoly, CRS = proj4string)
  }
  else {
    spP<- spPoly
  }
  areas<- unlist(lapply(spP@polygons, function(x) a<- x@area))
  return(areas)
}



# Sets the legend classes
#####################################################################
if (P_myMaxDens > 50000) {
  # Typically poultry
  myBreaks = c(0,1,25,50,100,250,500,1000,2500,5000,10000,1000000)
  mycols = c(
    rgb(255,255,233,max = 255), #0-1
    rgb(255,255,209,max = 255), #1-25
    rgb(254,247,178,max = 255), #25-50
    rgb(254,212,132,max = 255), #50-100
    rgb(253,160,74,max = 255), #100-250
    rgb(246,121,49,max = 255), #250-500
    rgb(239,76,36,max = 255), #500-1000
    rgb(226,26,27,max = 255), #1000 - 2500
    rgb(187,0,26,max = 255), #2500 - 5000
    rgb(143,0,29,max = 255), #5000
    rgb(107,0,28,max = 255)
  )
} else {
  if (P_myMaxDens > 30000) {
  # Typically pigs and small ruminants  
  myBreaks = c(0,1,5,10,20,50,100,250,1000000)
  mycols = c(
    rgb(255,250,210,max = 255), #0-1
    rgb(255,240,180,max = 255), #1-5
    rgb(254,200,130,max = 255), #5-10
    rgb(250,150,70,max = 255), #10-20
    rgb(240,80,40,max = 255), #20-50
    rgb(220,10,20,max = 255), #50-100
    rgb(160,0,30,max = 255), #100 - 250
    rgb(110,0,30,max = 255) # higher than 250 
  )
  } else {
    
    if (P_myMaxDens > 1000) {
      # Typically large ruminants
      myBreaks = c(0,1,5,10,20,50,100,250,1000000)
      mycols = c(
        rgb(255,250,210,max = 255), #0-1
        rgb(255,240,180,max = 255), #1-5
        rgb(254,200,130,max = 255), #5-10
        rgb(250,150,70,max = 255), #10-20
        rgb(240,80,40,max = 255), #20-50
        rgb(220,10,20,max = 255), #50-100
        rgb(160,0,30,max = 255), #100 - 250
        rgb(110,0,30,max = 255) # higher than 250 
      )
    } else {
      # Typically Horses, camels 
      myBreaks = c(0,1,2,4,8,16,32,64,1000000)
      mycols = c(
        rgb(255,250,210,max = 255), #0-1
        rgb(255,240,180,max = 255), #1-5
        rgb(254,200,130,max = 255), #5-10
        rgb(250,150,70,max = 255), #10-20
        rgb(240,80,40,max = 255), #20-50
        rgb(220,10,20,max = 255), #50-100
        rgb(160,0,30,max = 255), #100 - 250
        rgb(110,0,30,max = 255) # higher than 250 
      )
    }   
  }   
}

# Sets the legend classes for per capita maps
#####################################################################
if (P_myMaxDens > 50000) {
  myPcBreaks = c(0,1,2.5,5,10,25,50,100,250,500,1000,100000)
  myPccols = c(
    rgb(255,255,233,max = 255), #0-1
    rgb(255,255,209,max = 255), #1-25
    rgb(254,247,178,max = 255), #25-50
    rgb(254,212,132,max = 255), #50-100
    rgb(253,160,74,max = 255), #100-250
    rgb(246,121,49,max = 255), #250-500
    rgb(239,76,36,max = 255), #500-1000
    rgb(226,26,27,max = 255), #1000 - 2500
    rgb(187,0,26,max = 255), #2500 - 5000
    rgb(143,0,29,max = 255), #5000
    rgb(107,0,28,max = 255)
  )
} else {
  if (P_myMaxDens > 10000) {
    myPcBreaks = c(0,1,5,10,20,50,100,250,1000000)
    myPccols = c(
      rgb(255,250,210,max = 255), #0-1
      rgb(255,240,180,max = 255), #1-5
      rgb(254,200,130,max = 255), #5-10
      rgb(250,150,70,max = 255), #10-20
      rgb(240,80,40,max = 255), #20-50
      rgb(220,10,20,max = 255), #50-100
      rgb(160,0,30,max = 255), #100 - 250
      rgb(110,0,30,max = 255) # higher than 250 
    )
  } else {
    
    if (P_myMaxDens > 500) {
      myPcBreaks = c(0,1,5,10,20,50,100,250,1000000)
      myPccols = c(
        rgb(255,250,210,max = 255), #0-1
        rgb(255,240,180,max = 255), #1-5
        rgb(254,200,130,max = 255), #5-10
        rgb(250,150,70,max = 255), #10-20
        rgb(240,80,40,max = 255), #20-50
        rgb(220,10,20,max = 255), #50-100
        rgb(160,0,30,max = 255), #100 - 250
        rgb(110,0,30,max = 255) # higher than 250 
      )
    } else {
      myPcBreaks = c(0,1,2,4,8,16,32,64,1000000)
      myPccols = c(
        rgb(255,250,210,max = 255), #0-1
        rgb(255,240,180,max = 255), #1-5
        rgb(254,200,130,max = 255), #5-10
        rgb(250,150,70,max = 255), #10-20
        rgb(240,80,40,max = 255), #20-50
        rgb(220,10,20,max = 255), #50-100
        rgb(160,0,30,max = 255), #100 - 250
        rgb(110,0,30,max = 255) # higher than 250 
      )
      
      
    }   
    
    
    
  }   
}




# Loads objects used by all scripts
#####################################################################
# Reads the dbf containing files to be cropped
rasterOptions(tmpdir=P_TempFolder)
myFileExt = substr(P_myRasterList,nchar(P_myRasterList)-2,nchar(P_myRasterList))
if (myFileExt == "dbf"){ 
  G_myPredTable = read.dbf(P_myRasterList, as.is = T)
} else {
  G_myPredTable = read.csv(P_myRasterList, as.is = T)
}

G_myPredTable$PATH = as.character(G_myPredTable$PATH)
G_myPredTable$EXPATH = G_myPredTable$PATH
G_myPredTable$VARNAME = as.character(G_myPredTable$VARNAME)
G_myPredTable$TYPE = as.character(G_myPredTable$TYPE)
G_myUsedPredTable = subset(G_myPredTable, TYPE == "Pred" & MODEL == 1)
G_mySuitMaskPath = subset(G_myPredTable, TYPE == "MskSuit" & MODEL == 1)$PATH
G_mySuitMaskName = subset(G_myPredTable, TYPE == "MskSuit" & MODEL == 1)$VARNAME
G_myLandMaskPath = subset(G_myPredTable, TYPE == "MskLand" & MODEL == 1)$PATH
G_myAdminMaskPath = subset(G_myPredTable, TYPE == "MskAdmi" & MODEL == 1)$PATH
G_myAreaPath = subset(G_myPredTable, TYPE == "Area" & MODEL == 1)$PATH 
G_myStratPath = subset(G_myPredTable, TYPE == "Strat" & MODEL == 1)$PATH 
G_myDepIdPath = paste(P_myDepShape,".tif",sep = "")
G_myHpopPath = subset(G_myPredTable, TYPE == "Hpop" & MODEL == 1)$PATH













