##########################################################
# Random Forest
##########################################################

# Prepare reporting printouts
############################################
print("######################################################")
print("## Start of the Random Forest modelling")
print("######################################################")

# Prepare the output table with variables
############################################
myVarList = G_myUsedPredTable$VARNAME

# Start the loop over files 
############################################
for (nb in 1:P_nBoot){
#foreach(nb=1:P_nBoot)%dopar% {    
    
  # Loads the Dataframe to be analysed
  ############################################
  myOutDataFname = paste(P_myOutData,"/",P_TempFolder,"/",P_myOutName,"_BootStrap_",nb,".dbf", sep = "")
  myTDF = read.dbf(myOutDataFname)
  myTDF = na.omit(myTDF)
  myDF = subset(myTDF, IsModelPg == 1)
  rm(myTDF)
  
  # Log-transform the dependent variable
  ############################################
  myDF$DepLg = log10(myDF$CDens + 1)
  if (P_TypeDependent == 2) {myDF$DepLg = log10(myDF$Dens + 1)}
  if (P_TypeDependent == 3) {myDF$DepLg = log10(myDF$PCap)}
  if (P_TypeDependent == 4) {myDF$DepLg = log10(myDF$Abs + 1)}
  
  if(P_ForceUnsuitable == 1){    
    myDF$DepLg = ifelse(myDF[,G_mySuitMaskName] == 0, 0, myDF$DepLg)
  }
  
  
  print("######################################################")
  print(paste("## Processing Bootstrap", nb))
  print("######################################################")
  # https://www.nescent.org/wg/cart/images/9/91/Chapter6_March20.pdf
  
  myYVPos = match("DepLg",names(myDF))
  myXVPos = match(myVarList,names(myDF))

  
  # Gets the dependent variable
  myYD = myDF[,myYVPos]

  # Only keeps the unique dependent variables values
  myUniqueVec = unique(myYD)
  myUniquePos = match(myUniqueVec,myYD)
  if (P_UseUniqueValues == 1){
    myYD = myDF[myUniquePos,myYVPos]
    myXD = myDF[myUniquePos,myXVPos]
  } else {
    myYD = myDF[,myYVPos]
    myXD = myDF[,myXVPos]
  }
    
  # Sets the RF run parameters
  myRFVarN = ifelse(ceiling(length(myXD)/P_RF_VarFactor_F) < P_RF_VarFactor_Min, P_RF_VarFactor_Min,ceiling(length(myXD)/P_RF_VarFactor_F))
  myRFTreeN = ifelse(ceiling(length(myYD)/P_RF_Ntrees_F) < P_RF_Ntrees_Min, P_RF_Ntrees_Min,ceiling(length(myYD)/P_RF_Ntrees_F))
  myRFNodeS = ifelse(ceiling(length(myYD)/P_RF_NodeSize_F) < P_RF_NodeSize_Min, P_RF_NodeSize_Min,ceiling(length(myYD)/P_RF_NodeSize_F))
  
  # Sets the seed
  set.seed(2002)
  
  myRF = randomForest(x=myXD, y=myYD, mtry=myRFVarN
                      , ntree=myRFTreeN
                      , nodesize=myRFNodeS
                      , importance=TRUE, proximity=TRUE)
  
  myRFPath = paste(P_myOutData,"/",P_TempFolder,"/",P_myOutName,"_Bootstrap_",nb,".RF", sep = "")
  save(myRF,file = myRFPath)
  
  
} # End of loop through boostrappeed 
