##########################################################
# PREDICTIONS BY STRATIFICATION
##########################################################

# Open and restore the table with single and quadratic 
# coeficients
##########################################################
# Loads the list of predictors
myVarList = G_myUsedPredTable$VARNAME



print("######################################################")
print("## Start of the predictions")
print("######################################################")


# Start the loop by file, bootstrap and stratification
##########################################################
ne = 0
for (nb in 1:P_nBoot){
  print("######################################################")
  print(paste("## Processing Bootstrap", nb))
  print("######################################################")
  
  # loads the BRT model
  myRFPath = paste(P_myOutData,"/",P_TempFolder,"/",P_myOutName,"_Bootstrap_",nb,".RF", sep = "")
  load(myRFPath)
  
  # Assemble a rasterStack with the predictor covariates
  myPathList = as.list(G_myUsedPredTable$PATH)
  myStack = stack(myPathList)
  names(myStack) = myVarList
  # Apply the RF model to the raster stack
  myPred = predict(myStack, myRF, type = "response")
  # Saves the prediction
  myPredFilePath = paste(P_myOutData,"/",P_TempFolder,"/",P_myOutName,"_Nb",nb,"_RF.tif", sep = "")
  writeRaster(myPred,myPredFilePath, overwrite = T )
  ne = ne + 1
} # End of loops through bootstraps

print("######################################################")
print("## End of the predictions")
print(paste("## ",ne," layers have been created in ",P_TempFolder, sep = "")) 
print("######################################################")

# Clean the workspace
#########################################
#rm(MyACon)
#rm(myCoefBand)
#rm(myDims)
#rm(myEzBit)
#rm(myEzPath)
#rm(myEzTableSq)
#rm(myLgMaxDens)
#rm(myLoc)
#rm(myMaskBit)
#rm(myNameTablePath)
#rm(myPredFilePath)
#rm(myRedEzTable)
#rm(myRegDBSq)
#rm(mySubRedVarList)
#rm(mySumRedVec)
#rm(myTitle)
#rm(myVar)
#rm(myVarIndex)
#rm(myVarPath)
#rm(MyWCon)
#rm(Nband)
#rm(myBand)
#rm(myCoefSqBand)
#rm(myEzBand)
#rm(myEzName)
#rm(myEzTable)
#rm(myLengthVec)
#rm(myLgMinDens)
#rm(myLocBand)
#rm(myNameDB)
#rm(myOutDataFname)
#rm(myPVec)
#rm(myRegDB)
#rm(mySPixVec)
#rm(mySubVarList)
#rm(mySumVec)
#rm(myUnEz)
#rm(myVarBit)
#rm(myVarList)
#rm(myVarSq)
#rm(nb)
#rm(nf)
#rm(p)
#rm(x)
#rm(Ecozonation)



