##########################################################
# Check Inputs prior to the regression and outputs
##########################################################
# Description: Input check
##########################################################
# By: Marius Gilbert (mgilbert@ulb.ac.be)
##########################################################
# 13th Oct. 2011
##########################################################
print("#####################################################")
print("## Checking modelling inputs  ")
print("#####################################################")


if (!is.numeric(P_UseZeros)) {
  print("## Error: please use 1 or 0 to set whether zero values should be used or not")
  myErrorCode = myErrorCode + 1
  } else {
  if (P_UseZeros == 1) {
  print("## Info: Zero values will be used to train the models")
  } else {
  print ("## Info: Zero values will not be used to train the models")
  }
}

if (is.numeric(P_myMaxDens)) {
  # Open the dbf master file
  myOutDataFname = paste(P_myOutData,"/",P_TempFolder,"/",P_myOutName,"_Master.dbf", sep = "")
  myShapeDBF = read.dbf(myOutDataFname, as.is = T)
  myQT999Dens = quantile(myShapeDBF$CDens,0.999, na.rm = T)
  
  if (P_myMaxDens > (1.5*myQT999Dens[[1]])){
  print(paste("## Warning: the Qt 99.9% in the observed dataset is ",format(myQT999Dens[[1]], digits = 2), "animals / sq. km", sep = " "))
  print("## Warning: your input max. density is more than 50% higher than Qt 99.9%")
  print("## Warning: verify your inputs")
  }

  if (P_myMaxDens < (0.5*myQT999Dens[[1]])){
    print(paste("## Warning: the Qt 99.9% in the observed dataset is ",format(myQT999Dens[[1]], digits = 2), "animals / sq. km", sep = " "))
    print("## Warning: your input max. density is lower than 50% of Qt 99.9%")
    print("## Warning: verify your inputs")
  }
  
  
  print(paste("## Info: your maximum density is ", P_myMaxDens, " animals / sq. km"))
  } else {
  print("## Error: the maximum density is not numeric")
  myErrorCode = myErrorCode + 1
}


if (!is.numeric(P_UseSuitMask)) {
  print("## Error: please use 1 or 0 to set whether suitability masking should be applied to predictions")
  } else {
  if (P_UseSuitMask == 1) {
  print("## Info: Suitability masking will be applied")
  } else {
  print ("## Info: Suitability masking will not be applied")
  }
}

if (!is.numeric(P_UseAdminMask)) {
  print("## Error: please use 1 or 0 to set whether administrative masking should be applied to predictions")
  } else {
  if (P_UseAdminMask == 1) {
  print("## Info: Administrative masking will be applied")
  } else {
  print ("## Info: Administrative masking will not be applied")
  }
}

print("#####################################################")
print("## End of checking modelling inputs  ")
print("#####################################################")


