#######################################################'
## Title Models Goodness of fit metrics 
## Author: Daniele Da Re 
######################################################'

##------------Setwd and load pkgs------
setwd("/home/ddare/Data/nextcloud/GLW20bts_models_output/") 
library(foreign)
library(ggplot2)
library(dplyr)
library(data.table)

Rmse <- function(error)
{
  sqrt(mean(error^2))
}

##------------Comupte Pred Vs Obs Pearson's r and RMSE ---------
#load CHdis_IRR and CHdis_REG results for both sampling methods
ch_resDisIRR<-read.dbf("CHdis_IRRrp/1_outputs/Ch_GOFData.dbf")#random point
ch_resDisREG<-read.dbf("CHdis_REGrp/1_outputs/Ch_GOFData.dbf")
ch_resDisIRRav<-read.dbf("CHdis_IRRav/1_outputs/Ch_GOFData.dbf")#averaged
ch_resDisREGav<-read.dbf("CHdis_REGav/1_outputs/Ch_GOFData.dbf")
ch_resSdsIRR<-read.dbf("CHsds_IRRrp/1_outputs/Ch_GOFData.dbf")#random point
ch_resSdsREG<-read.dbf("CHsds_REGrp/1_outputs/Ch_GOFData.dbf")
ch_resSdsIRRav<-read.dbf("CHsds_IRRav/1_outputs/Ch_GOFData.dbf")#averaged
ch_resSdsREGav<-read.dbf("CHsds_REGav/1_outputs/Ch_GOFData.dbf")
ch_resVilIRR<-read.dbf("CHvil_IRRrp/1_outputs/Ch_GOFData.dbf")#random point
# ch_resVilREG<-read.dbf("CHvil_REGrp/1_outputs/Ch_GOFData.dbf")
ch_resVilIRRav<-read.dbf("CHvil_IRRav/1_outputs/Ch_GOFData.dbf")#averaged
# ch_resVilREGav<-read.dbf("CHvil_REGav/1_outputs/Ch_GOFData.dbf")

dk_resDisIRR<-read.dbf("DKdis_IRRrp/1_outputs/Dk_GOFData.dbf")#random point
dk_resDisREG<-read.dbf("DKdis_REGrp/1_outputs/Dk_GOFData.dbf")
dk_resDisIRRav<-read.dbf("DKdis_IRRav/1_outputs/Dk_GOFData.dbf")#averaged
dk_resDisREGav<-read.dbf("DKdis_REGav/1_outputs/Dk_GOFData.dbf")
dk_resSdsIRR<-read.dbf("DKsds_IRRrp/1_outputs/Dk_GOFData.dbf")#random point
dk_resSdsREG<-read.dbf("DKsds_REGrp/1_outputs/Dk_GOFData.dbf")
dk_resSdsIRRav<-read.dbf("DKsds_IRRav/1_outputs/Dk_GOFData.dbf")#averaged
dk_resSdsREGav<-read.dbf("DKsds_REGav/1_outputs/Dk_GOFData.dbf")
dk_resVilIRR<-read.dbf("DKvil_IRRrp/1_outputs/Dk_GOFData.dbf")#random point
# dk_resVilREG<-read.dbf("DKvil_REGrp/1_outputs/Dk_GOFData.dbf")
dk_resVilIRRav<-read.dbf("DKvil_IRRav/1_outputs/Dk_GOFData.dbf")#averaged
# dk_resVilREGav<-read.dbf("DKvil_REGav/1_outputs/Dk_GOFData.dbf")

#create Id col and stack together
ch_resDisIRR$model<-rep("ch_resDisIRR", nrow(ch_resDisIRR))
ch_resDisREG$model<-rep("ch_resDisREG", nrow(ch_resDisREG))
ch_resDisIRRav$model<-rep("ch_resDisIRRav", nrow(ch_resDisIRRav))
ch_resDisREGav$model<-rep("ch_resDisREGav", nrow(ch_resDisREGav))
ch_resSdsIRR$model<-rep("ch_resSdsIRR", nrow(ch_resSdsIRR))
ch_resSdsREG$model<-rep("ch_resSdsREG", nrow(ch_resSdsREG))
ch_resSdsIRRav$model<-rep("ch_resSdsIRRav", nrow(ch_resSdsIRRav))
ch_resSdsREGav$model<-rep("ch_resSdsREGav", nrow(ch_resSdsREGav))
ch_resVilIRR$model<-rep("ch_resVilIRR", nrow(ch_resVilIRR))
ch_resVilIRRav$model<-rep("ch_resVilIRRav", nrow(ch_resVilIRRav))

ch_resDisIRR$species<-rep("CH", nrow(ch_resDisIRR))
ch_resDisREG$species<-rep("CH", nrow(ch_resDisREG))
ch_resDisIRRav$species<-rep("CH", nrow(ch_resDisIRRav))
ch_resDisREGav$species<-rep("CH", nrow(ch_resDisREGav))
ch_resSdsIRR$species<-rep("CH", nrow(ch_resSdsIRR))
ch_resSdsREG$species<-rep("CH", nrow(ch_resSdsREG))
ch_resSdsIRRav$species<-rep("CH", nrow(ch_resSdsIRRav))
ch_resSdsREGav$species<-rep("CH", nrow(ch_resSdsREGav))
ch_resVilIRR$species<-rep("CH", nrow(ch_resVilIRR))
ch_resVilIRRav$species<-rep("CH", nrow(ch_resVilIRRav))

ch_resDisIRR$sampling<-rep("rp", nrow(ch_resDisIRR))
ch_resDisREG$sampling<-rep("rp", nrow(ch_resDisREG))
ch_resDisIRRav$sampling<-rep("av", nrow(ch_resDisIRRav))
ch_resDisREGav$sampling<-rep("av", nrow(ch_resDisREGav))
ch_resSdsIRR$sampling<-rep("rp", nrow(ch_resSdsIRR))
ch_resSdsREG$sampling<-rep("rp", nrow(ch_resSdsREG))
ch_resSdsIRRav$sampling<-rep("av", nrow(ch_resSdsIRRav))
ch_resSdsREGav$sampling<-rep("av", nrow(ch_resSdsREGav))
ch_resVilIRR$sampling<-rep("rp", nrow(ch_resVilIRR))
ch_resVilIRRav$sampling<-rep("av", nrow(ch_resVilIRRav))

dk_resDisIRR$model<-rep("dk_resDisIRR", nrow(dk_resDisIRR))
dk_resDisREG$model<-rep("dk_resDisREG", nrow(dk_resDisREG))
dk_resDisIRRav$model<-rep("dk_resDisIRRav", nrow(dk_resDisIRRav))
dk_resDisREGav$model<-rep("dk_resDisREGav", nrow(dk_resDisREGav))
dk_resSdsIRR$model<-rep("dk_resSdsIRR", nrow(dk_resSdsIRR))
dk_resSdsREG$model<-rep("dk_resSdsREG", nrow(dk_resSdsREG))
dk_resSdsIRRav$model<-rep("dk_resSdsIRRav", nrow(dk_resSdsIRRav))
dk_resSdsREGav$model<-rep("dk_resSdsREGav", nrow(dk_resSdsREGav))
dk_resVilIRR$model<-rep("dk_resVilIRR", nrow(dk_resVilIRR))
dk_resVilIRRav$model<-rep("dk_resVilIRRav", nrow(dk_resVilIRRav))

dk_resDisIRR$species<-rep("DK", nrow(dk_resDisIRR))
dk_resDisREG$species<-rep("DK", nrow(dk_resDisREG))
dk_resDisIRRav$species<-rep("DK", nrow(dk_resDisIRRav))
dk_resDisREGav$species<-rep("DK", nrow(dk_resDisREGav))
dk_resSdsIRR$species<-rep("DK", nrow(dk_resSdsIRR))
dk_resSdsREG$species<-rep("DK", nrow(dk_resSdsREG))
dk_resSdsIRRav$species<-rep("DK", nrow(dk_resSdsIRRav))
dk_resSdsREGav$species<-rep("DK", nrow(dk_resSdsREGav))
dk_resVilIRR$species<-rep("DK", nrow(dk_resVilIRR))
dk_resVilIRRav$species<-rep("DK", nrow(dk_resVilIRRav))

dk_resDisIRR$sampling<-rep("rp", nrow(dk_resDisIRR))
dk_resDisREG$sampling<-rep("rp", nrow(dk_resDisREG))
dk_resDisIRRav$sampling<-rep("av", nrow(dk_resDisIRRav))
dk_resDisREGav$sampling<-rep("av", nrow(dk_resDisREGav))
dk_resSdsIRR$sampling<-rep("rp", nrow(dk_resSdsIRR))
dk_resSdsREG$sampling<-rep("rp", nrow(dk_resSdsREG))
dk_resSdsIRRav$sampling<-rep("av", nrow(dk_resSdsIRRav))
dk_resSdsREGav$sampling<-rep("av", nrow(dk_resSdsREGav))
dk_resVilIRR$sampling<-rep("rp", nrow(dk_resVilIRR))
dk_resVilIRRav$sampling<-rep("av", nrow(dk_resVilIRRav))

#stack irregular PSUs
res_stack_dis<-rbind(ch_resDisIRR, ch_resDisIRRav, dk_resDisIRR, dk_resDisIRRav)
res_stack_dis$AdmLev<-rep("Districts", nrow(res_stack_dis))
res_stack_sds<-rbind(ch_resSdsIRR, ch_resSdsIRRav, dk_resSdsIRR, dk_resSdsIRRav) 
res_stack_sds$AdmLev<-rep("Subdistricts", nrow(res_stack_sds))
res_stack_vil<-rbind(ch_resVilIRR, ch_resVilIRRav, dk_resVilIRR, dk_resVilIRRav)
res_stack_vil$AdmLev<-rep("Villages", nrow(res_stack_vil))

dis_reg<-rbind(ch_resDisREG, ch_resDisREGav, dk_resDisREG, dk_resDisREGav)
dis_reg$AdmLev<-rep("Districts", nrow(dis_reg))
sds_reg<-rbind(ch_resSdsREG, ch_resSdsREGav, dk_resSdsREG, dk_resSdsREGav)
sds_reg$AdmLev<-rep("Subdistricts", nrow(sds_reg))
res_stack_REG<-rbind(dis_reg,sds_reg)
res_stack_REG$shape<-rep("REG", nrow(res_stack_REG))
res_stack_REG$area_cat<-ifelse(res_stack_REG$SAreaKm < 400 ,"67 km² - Regular grid", "557 km² - Regular grid")
                               
# lets subdivide the dataset in three groub based on the Areas bin distribution: <100, 100-5000, >5000
res_stack_dis$area_cat<-ifelse(res_stack_dis$SAreaKm <= 500 ,"500 km²", 
                           ifelse(res_stack_dis$SAreaKm > 1000, ">1000 km²",
                                  ifelse(res_stack_dis$SAreaKm > 500   | res_stack_dis$SAreaKm <= 1000, "500-1000 km²")))
res_stack_dis$area_cat<-as.factor(res_stack_dis$area_cat)
levels(res_stack_dis$area_cat)

res_stack_sds$area_cat<-ifelse(res_stack_sds$SAreaKm <= 100 ,"100 km²", 
                               ifelse(res_stack_sds$SAreaKm > 200, ">200 km²",
                                      ifelse(res_stack_sds$SAreaKm > 100   | res_stack_sds$SAreaKm <= 200, "100-200 km²")))
res_stack_sds$area_cat<-as.factor(res_stack_sds$area_cat)
levels(res_stack_sds$area_cat)

res_stack_vil$area_cat<-ifelse(res_stack_vil$SAreaKm <= 10 ,"10 km²", 
                               ifelse(res_stack_vil$SAreaKm > 20, ">20 km²",
                                      ifelse(res_stack_vil$SAreaKm > 10   | res_stack_vil$SAreaKm <= 20, "10-20 km²")))
res_stack_vil$area_cat<-as.factor(res_stack_vil$area_cat)
levels(res_stack_vil$area_cat)


res_stack<-rbind(res_stack_dis, res_stack_sds, res_stack_vil)
res_stack_REG

res_stack$shape<-rep("IRR", nrow(res_stack))

#convert ObsDens and SRFPolDens to Log10
res_stack$ObsDens<-log10(res_stack$ObsDens+1)
res_stack$SRFPolDens<-log10(res_stack$SRFPolDens+1)

res_stack_REG$ObsDens<-log10(res_stack_REG$ObsDens+1)
res_stack_REG$SRFPolDens<-log10(res_stack_REG$SRFPolDens+1)

#convert bootstraps into factors 
res_stack$Bnum<-as.factor(res_stack$Bnum)
res_stack_REG$Bnum<-as.factor(res_stack_REG$Bnum)

##compute pearson's r and RMSE between predicted and observed value at different ASR
res_stack$area_cat<-as.factor(res_stack$area_cat)
res_stack_REG$area_cat<-as.factor(res_stack_REG$area_cat)

na.omit(res_stack)%>%
  group_by(Bnum, area_cat, model, sampling, species) %>%
  summarize(COR=cor(ObsDens,SRFPolDens, method = "pearson", use="complete.obs"), rmse=na.omit(Rmse(SRFPolDens-ObsDens))) -> res_stack_IRR_gof
res_stack_IRR_gof<-as.data.frame(res_stack_IRR_gof)
summary(res_stack_IRR_gof)

na.omit(res_stack_REG)%>%
  group_by(Bnum, area_cat, model, sampling, species) %>%
  summarize(COR=cor(ObsDens,SRFPolDens, method = "pearson", use="complete.obs"), rmse=na.omit(Rmse(SRFPolDens-ObsDens))) -> res_stack_REG_gof
res_stack_REG_gof<-as.data.frame(res_stack_REG_gof)
summary(res_stack_REG_gof) #get NAs check 

na.omit(res_stack_IRR_gof)%>%
  group_by(model) %>%
  summarize(COR=mean(COR), rmse=mean(rmse)) -> res_stack_IRR_gof_summary
res_stack_IRR_gof_summary<-as.data.frame(res_stack_IRR_gof_summary)
summary(res_stack_IRR_gof_summary)

models<-c("Overall Districts Model", "Overall Subdistricts Model",  "Overall Villages Model")
res_stack_IRR_gof_summary$area_cat<-rep(models, each=2)

na.omit(res_stack_REG_gof)%>%
  group_by(model) %>%
  summarize(COR=mean(COR), rmse=mean(rmse)) -> res_stack_REG_gof_summary
res_stack_REG_gof_summary<-as.data.frame(res_stack_REG_gof_summary)
summary(res_stack_REG_gof_summary)
models<-c("557 km² - Regular grid", "67 km² - Regular grid")
res_stack_REG_gof_summary$area_cat<-rep(models, each=2)

spec<-c("CH", "DK")
sampl<-c("rp", "av")
res_stack_IRR_gof_summary$sampling<-rep(sampl, 6)
res_stack_IRR_gof_summary$species<-rep(spec, each = 6)
res_stack_REG_gof_summary$sampling<-rep(sampl, 4)
res_stack_REG_gof_summary$species<-rep(spec, each = 4)

res_stack_IRR_gof$area_cat <- as.character(res_stack_IRR_gof$area_cat)
res_stack_REG_gof$area_cat <- as.character(res_stack_REG_gof$area_cat)
res_stack_IRR_gof_summary$area_cat<- as.character(res_stack_IRR_gof_summary$area_cat)
res_stack_REG_gof_summary$area_cat<- as.character(res_stack_REG_gof_summary$area_cat)

res_stack_gof<-rbind(res_stack_IRR_gof[, 2:ncol(res_stack_IRR_gof)], res_stack_REG_gof[, 2:ncol(res_stack_REG_gof)],res_stack_IRR_gof_summary,  res_stack_REG_gof_summary)

summary(res_stack_gof)

#Get censLev column 
censLev_df<-rbind(res_stack[,c(17,20)], res_stack_REG[,c(17,20)])
censLev_df<-unique(censLev_df)

res_stack_gof<-left_join(res_stack_gof, censLev_df, by='model')
res_stack_gof$area_cat <- factor(res_stack_gof$area_cat,c("10 km²", "10-20 km²", ">20 km²", "Overall Villages Model", "100 km²", "100-200 km²", ">200 km²", "67 km² - Regular grid", "Overall Subdistricts Model", "500 km²", "500-1000 km²", ">1000 km²",  "557 km² - Regular grid", "Overall Districts Model"))

summary(res_stack_gof$area_cat)
# View(res_stack_gof)

##----------- Plot  Pred Vs Obs Pearson's r and RMSE -------------------
ggplot(res_stack_gof, aes(x = area_cat, y = COR, fill= AdmLev)) +
  geom_point() +
  geom_bar(data = res_stack_gof, stat = "summary", fun.y = "mean", alpha = .3)+
  theme(text=element_text(size=10, 
                          #       family="Comic Sans MS"))
                          #       family="CM Roman"))
                          #       family="Serif"))
                          #       family="Sans"))
                          family="TT Times New Roman"))+
  facet_wrap(~species + sampling, ncol=2)+
  theme_minimal()+
  theme(axis.text.x=element_text(angle=60,hjust=1))+
  labs(x ="Sampling units area", y = "Pearson's r")


ggplot(res_stack_gof, aes(x = area_cat, y = rmse, fill= AdmLev)) +
  geom_point() +
  geom_bar(data = res_stack_gof, stat = "summary", fun.y = "mean", alpha = .3)+
  theme(text=element_text(size=10, 
                          #       family="Comic Sans MS"))
                          #       family="CM Roman"))
                          #       family="Serif"))
                          #       family="Sans"))
                          family="TT Times New Roman"))+
  facet_wrap(~species + sampling, ncol=2)+
  theme_minimal()+
  theme(axis.text.x=element_text(angle=60,hjust=1))+
  labs(x ="Sampling units area", y = "RMSE")


##----------- Plot Thailand District and Subdistrict map ####
setwd("/home/ddare/Data/nextcloud/")
thai <-shapefile("GLW20bts_models_output/04_Results/Thai_Country_v2.shp")
obs_sds<-shapefile("pelican_home/GLW_test/20171207/MultipleGLWCountry_Models/01_Response/DKsds_IRRrp.shp")
obs_dis<-shapefile("pelican_home/GLW_test/20171207/MultipleGLWCountry_Models/01_Response/DKdis_IRRrp.shp")

ch_obs_sds<-shapefile("pelican_home/GLW_test/20171207/MultipleGLWCountry_Models/01_Response/CHsds_IRRrp.shp")
ch_obs_dis<-shapefile("pelican_home/GLW_test/20171207/MultipleGLWCountry_Models/01_Response/CHsdis_IRRrp.shp")

##load raster template
require(raster)
r.temp<-raster("GLW20bts_models_output/DKdis_IRRrp/1_outputs/Dk_Mn.tif")
r.hollow<-r.temp
r.hollow[r.hollow]<-0

## rasterize
require(velox)
require(sf)
r.temp<-velox(r.temp)

r.obs_sds<- r.temp$copy()
r.obs_sds$rasterize(obs_sds, field="Log10Dens", band=1, background = 0)
r.obs_sds <- r.obs_sds$as.RasterLayer(band=1)

r.obs_dis<- r.temp$copy()
r.obs_dis$rasterize(obs_dis, field="Log10Dens", band=1, background = 0)
r.obs_dis <- r.obs_dis$as.RasterLayer(band=1)

r.obs_ch_sds<- r.temp$copy()
r.obs_ch_sds$rasterize(ch_obs_sds, field="Log10Dens", band=1, background = 0)
r.obs_ch_sds <- r.obs_ch_sds$as.RasterLayer(band=1)

r.obs_ch_dis<- r.temp$copy()
r.obs_ch_dis$rasterize(ch_obs_dis, field="Log10Dens", band=1, background = 0)
r.obs_ch_dis <- r.obs_ch_dis$as.RasterLayer(band=1)

#stack
thai_obs<-stack(r.obs_ch_dis, r.obs_ch_sds, r.obs_dis, r.obs_sds)
thai_obs<-mask(thai_obs, thai)
costline<- as(thai, "SpatialLines") 
r.back<-r.hollow

#plot
require(rasterVis)
myBreaks= c(0, 1, 1.7, 2, 2.4, 2.7, 3, 3.4, 3.7, 4, 6)
mycols = colorRampPalette(c(
  rgb(255,255,233,max = 255),
  rgb(255,255,209,max = 255),
  rgb(254,247,178,max = 255),
  rgb(254,212,132,max = 255),
  rgb(253,160,74,max = 255), 
  rgb(246,121,49,max = 255), 
  rgb(239,76,36,max = 255), 
  rgb(226,26,27,max = 255), 
  rgb(187,0,26,max = 255), 
  rgb(143,0,29,max = 255), 
  rgb(107,0,28,max = 255)
))

myColorkey <- list(at=myBreaks, ## where the colors change
                   labels=list(labels=myBreaks, ##what to print
                               at=myBreaks))    ##where to print


thai_sds<-levelplot(thai_obs, names.attr=c("","", "",""), scales=list(draw=FALSE), layout=c(2, 2),
                    ylab=c("Ducks", "Chickens"), xlab=c("Districts", "Sub-Districts"),
                    colorkey= myColorkey, contour = FALSE, margin = FALSE, at = myBreaks, 
                    col.regions = mycols)

thai_sds


thai_sds +  layer({
    xs <- seq(800000, 1000000, by=5000)
    grid.rect(x=xs, y=1000000,
              width=100, height=30,
              gp=gpar(fill=rep(c('transparent', 'black'), 2)),
              default.units='native')
    grid.text(x= xs - 50, y=1000010, seq(0, 5000, by=1000),
              gp=gpar(cex=0.5), rot=30,
              default.units='native')
  })

layout.scale.bar()

#, par.settings=list(panel.background=list(col="skyblue")
thai_sds<-thai_sds +layer(sp.polygons(masked_prov, fill='darkgray',col='gray')) 
thai_sds<-thai_sds +layer(sp.lines(costline, col='gray')) 
thai_sds

#ggsave("thai_sds.tiff",  height=8, width=12, units="in", dpi=300, plot= thai_sds, path = "D:/nextcloud/GLW20bts_models_output/04_Results/output_paper/")

tiff(file='thai_sds.tiff', height=10, width=14, units="in", res=300 ) 
thai_sds
dev.off()  



