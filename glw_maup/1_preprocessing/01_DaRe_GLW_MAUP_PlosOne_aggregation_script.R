#######################################################
## Downscaling livestock census data using multivariate predictive models: sensitivity to modifiable areal unit problem
## Author: Daniele Da Re 
## Poultry Census data Aggregation   
#####################################################

## -------- Set wd and load pkgs ----
setwd("/home/ddare/Documents/DanieleDaRe/UCL/Paper1/september2017/NewAggregations")
library(rgdal)
library(sp)
library(raster)
library(dismo)
library(readr)
library(data.table) 
library(deldir)
library(rgeos)
library(readr)
library(data.table) 
library(geoR)

## -------- Load data ----
# Load Thailand administrative units data
thai<-shapefile("Thai_Country_v2.shp")
thai_prov<-shapefile("Thai_Provinces_v2.shp")
thai_distr<-shapefile("Thai_Districts_v2.shp")
thai_Subdistr<-shapefile("Thailand_Subdistrict.shp")
villages<-shapefile("/home/ddare/Documents/DanieleDaRe/UCL/Paper1/september2017/Data/Celia_data/VillageCode_Intersect_THA_sh.shp")

# Load Thailand poultry census data
files <- list.files(path = "C:/Users/ddare/Documents/DanieleDaRe/UCL/Paper1/september2017/NewAggregations/2010_census",pattern = ".csv", full.names = TRUE)
print(files)
temp <- lapply(files, fread, sep=",")
owners <- rbindlist( temp )
owners <- owners[,c(1:4,13,19)]
owners$id<-1:nrow((owners))
owners$vill_code<-owners$SD_CODE*100 + owners$VILL_CODE
owners$VILL_CODE<-as.factor(owners$VILL_CODE)
owners$vill_code<-as.factor(owners$vill_code)
owners$distr_code <- substr(owners$vill_code, 0, 4)
owners$prov_code <- substr(owners$vill_code, 0, 2)

# Split dataset in chicken and ducks owners
ch_owners<-owners[,-c(6)]
head(ch_owners)
nrow(ch_owners)
ch_owners<-na.omit(ch_owners) #remove NAs
nrow(ch_owners) #NO NAs

dk_owners<-owners[,-c(5)]
head(dk_owners)
nrow(dk_owners)
dk_owners<-na.omit(dk_owners) #remove NAs
nrow(dk_owners) #NO NAs

## -------- Data Cleaning ----
#First check on the raw village dataset
nrow(villages)#62'142
length(levels(as.factor(villages@data$VILL_CODE))) #57'810 unic villages id
#So we have 4332 village with a non unic id

#Extract duplicates 
vill_repl<-villages@data[(duplicated(villages@data$VILL_CODE, fromLast = FALSE) | duplicated(villages@data$VILL_CODE, fromLast = TRUE)),]
dim(vill_repl)
which(duplicated(vill_repl$VILL_CODE))

# Extract rows that appears only once 
vill_uniq<-villages@data
vill_uniq<-vill_uniq[!(vill_uniq$VILL_CODE %in% vill_repl$VILL_CODE),]
dim(vill_uniq)
which(duplicated(vill_uniq$VILL_CODE))

# Randomly select (and keep) one record from each duplicate using the *stratified* function. 
stratified <- function(df, group, size, select = NULL, 
                       replace = FALSE, bothSets = FALSE) {
  if (is.null(select)) {
    df <- df
  } else {
    if (is.null(names(select))) stop("'select' must be a named list")
    if (!all(names(select) %in% names(df)))
      stop("Please verify your 'select' argument")
    temp <- sapply(names(select),
                   function(x) df[[x]] %in% select[[x]])
    df <- df[rowSums(temp) == length(select), ]
  }
  df.interaction <- interaction(df[group], drop = TRUE)
  df.table <- table(df.interaction)
  df.split <- split(df, df.interaction)
  if (length(size) > 1) {
    if (length(size) != length(df.split))
      stop("Number of groups is ", length(df.split),
           " but number of sizes supplied is ", length(size))
    if (is.null(names(size))) {
      n <- setNames(size, names(df.split))
      message(sQuote("size"), " vector entered as:\n\nsize = structure(c(",
              paste(n, collapse = ", "), "),\n.Names = c(",
              paste(shQuote(names(n)), collapse = ", "), ")) \n\n")
    } else {
      ifelse(all(names(size) %in% names(df.split)),
             n <- size[names(df.split)],
             stop("Named vector supplied with names ",
                  paste(names(size), collapse = ", "),
                  "\n but the names for the group levels are ",
                  paste(names(df.split), collapse = ", ")))
    }
  } else if (size < 1) {
    n <- round(df.table * size, digits = 0)
  } else if (size >= 1) {
    if (all(df.table >= size) || isTRUE(replace)) {
      n <- setNames(rep(size, length.out = length(df.split)),
                    names(df.split))
    } else {
      message(
        "Some groups\n---",
        paste(names(df.table[df.table < size]), collapse = ", "),
        "---\ncontain fewer observations",
        " than desired number of samples.\n",
        "All observations have been returned from those groups.")
      n <- c(sapply(df.table[df.table >= size], function(x) x = size),
             df.table[df.table < size])
    }
  }
 
   temp <- lapply(
    names(df.split),
    function(x) df.split[[x]][sample(df.table[x],
                                     n[x], replace = replace), ])
  set1 <- do.call("rbind", temp)
  
  if (isTRUE(bothSets)) {
    set2 <- df[!rownames(df) %in% rownames(set1), ]
    list(SET1 = set1, SET2 = set2)
  } else {
    set1
  }
}

vill_repl_filtered<-stratified(vill_repl, "VILL_CODE", 1) #randomly select one duplicate row by "dup" field
which(duplicated(vill_repl_filtered$VILL_CODE))
dim(vill_repl)
dim(vill_repl_filtered)

# Now lets add the randomly selected rows from the duplicate df
village_clean<-rbind(vill_uniq, vill_repl_filtered)
dim(villages)
dim(village_clean)
which(duplicated(village_clean$VILL_CODE))

# Look for duplicate coordinates
require(geoR)
vill_spdf<-village_clean
vill_spdf<-as.geodata(vill_spdf, coords.col=7:8, covar.col= c(3,15), na.action = "none")
vill_coordRep<-dup.coords(vill_spdf) #find records with duplicates coordinates
nrow(vill_coordRep)
#we have 29 villages with duplicate coordinates

#now we random select one of the records and we delete it from the cill_clean df
vill_coordRep_filt<-stratified(vill_coordRep, "dup", 1)

dim(village_clean)
village_clean<-village_clean[!(village_clean$VILL_CODE %in% vill_coordRep_filt$VILL_CODE),]
dim(village_clean)
which(duplicated(village_clean$VILL_CODE))
village_clean$VILL_CODE<-as.factor(village_clean$VILL_CODE)
dim(village_clean)

# Compare two dfs to find the rows in df1 that are not present in df2
nrow(aggregate(owners$ALL_CHICKEN~ owners$vill_code, data=owners, FUN=sum)) #73618
nrow(village_clean) #57796

#we have a difference between the villages database and the virtual villages present in the owners df. 
#lets try to find the villages in the village_clean df one that does not match villages in the owners df

# Villages of village_clean df that are present also in the owners df
pres<-owners[owners$vill_code %in% village_clean$VILL_CODE,]
nrow(pres) #2309865
head(pres)

# Villages of village_clean df that are NOT present also in the owners df
abs<-owners[!owners$vill_code %in% village_clean$VILL_CODE,]
nrow(abs) #860348
head(abs)

#nrow(pres)+nrow(abs) = nrow(owners)

#ok, now lets collapse the dataset by vill_code in order to get the 
pres_CHcount<-aggregate(pres$ALL_CHICKEN ~ pres$vill_code,data=pres,sum)
nrow(pres_CHcount)#53301
colnames(pres_CHcount)<-c("vill_code","ALL_CHICKEN")
summary(pres_CHcount$ALL_CHICKEN)

abs_CHcount<-aggregate(abs$ALL_CHICKEN ~ abs$vill_code,data=abs,sum)
nrow(abs_CHcount) #20317
head(abs_CHcount)
colnames(abs_CHcount)<-c("VILL_CODE","ALL_CHICKEN")
abs_CHcount$distr_code <- substr(abs_CHcount$VILL_CODE, 0, 4)
abs_CHcount$prov_code <- abs_CHcount(abs$VILL_CODE, 0, 2)
hist(as.numeric(abs_CHcount$prov_code))

nrow(abs_CHcount)
abs_CHcountXY<-merge(abs_CHcount, village_clean, by="VILL_CODE")
nrow(abs_CHcountXY) # 0 rows!!!!! I dont have XY cord fot these villages

# Let's geolocate the presence
colnames(pres_CHcount)<-c("VILL_CODE","ALL_CHICKEN")
pres_CHcountXY<-merge(pres_CHcount, village_clean, by="VILL_CODE")
nrow(pres_CHcountXY)

coordinates(pres_CHcountXY)<-~ X+ Y
class(pres_CHcountXY)

summary(pres_CHcountXY)
shapefile(pres_CHcountXY, "pres_CHcountXY.shp")

# pres_CHcountXY will be our starting dataset, that is already aggregated at villages level

## Let's do the same for ducks
pres_DKcount<-aggregate(pres$ALL_DUCK ~ pres$vill_code,data=pres,sum)
nrow(pres_DKcount)#53301
colnames(pres_DKcount)<-c("vill_code","ALL_DUCK")
summary(pres_DKcount$ALL_CHICKEN)

#lets geolocate the presence
colnames(pres_DKcount)<-c("VILL_CODE","ALL_DUCK")
pres_DKcountXY<-merge(pres_DKcount, village_clean, by="VILL_CODE")
nrow(pres_DKcountXY)

coordinates(pres_DKcountXY)<-~ X+ Y
class(pres_DKcountXY)
summary(pres_DKcountXY)
shapefile(pres_DKcountXY, "pres_DKcountXY.shp")

## --------  AGGREGATION (pt1) of IRRegular boundaries census units ----
#We will use  pres_CHcountXY and pres_DKcountXY files to aggregate at different census levels 
names(pres_CHcountXY)
chick_vill<-pres_CHcountXY@data[,1:2]
colnames(chick_vill)<-c("VILL_CODE", "count")
head(chick_vill)
chick_subDistr<-aggregate(pres_CHcountXY@data$ALL_CHICKEN ~ pres_CHcountXY@data$SD_CODE,data=pres_CHcountXY@data,sum)
colnames(chick_subDistr)<-c("SD_CODE", "count")
chick_distr<-aggregate(pres_CHcountXY@data$ALL_CHICKEN ~ pres_CHcountXY@data$D_CODE,data=pres_CHcountXY@data,sum)
colnames(chick_distr)<-c("D_CODE", "count")

names(pres_DKcountXY)
duck_vill<-pres_DKcountXY@data[,1:2]
colnames(duck_vill)<-c("VILL_CODE", "count")
head(duck_vill)
duck_subDistr<-aggregate(pres_DKcountXY@data$ALL_DUCK ~ pres_DKcountXY@data$SD_CODE,data=pres_DKcountXY@data,sum)
colnames(duck_subDistr)<-c("SD_CODE", "count")
duck_distr<-aggregate(pres_DKcountXY@data$ALL_DUCK ~ pres_DKcountXY@data$D_CODE,data=pres_DKcountXY@data,sum)
colnames(duck_distr)<-c("D_CODE", "count")

# Voronoi polygon to simumate villages irregular boundaries
# Let's check if CH and Dk villages are the same villages
nrow(pres_CHcountXY@data)
nrow(pres_DKcountXY@data)
x<-pres_CHcountXY@data[!pres_CHcountXY@data$vill_code %in% pres_DKcountXY@data$VILL_CODE,]
nrow(x)#perfect, we have the same villages, so to generate voronoi polygon it does not matter which spdf we choose

require(dismo)
require(deldir)
require(rgeos)
vor <- voronoi(pres_CHcountXY)# quite time consuming
crs(vor)<- "+proj=utm +zone=47 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"
#vor_villages<-vor[thai,] #time consuming step, I did it in ArcGIS
vor_villages<-shapefile("voronoi_villages.shp")
crs(vor_villages)<- "+proj=utm +zone=47 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"
names(vor_villages)
head(vor_villages@data)
vor_villages<-vor_villages[,c(1,8:10)]
vor_villages@data$area_km<- gArea(vor_villages, byid=TRUE)/1000000
colnames(vor_villages@data)<-c("VILL_CODE" ,"SD_CODE", "D_CODE"  ,"P_CODE" , "area_km")

## Merge count CH and DK census data to villages voronoi polygon
vor_villagesCH<-merge(vor_villages, pres_CHcountXY, by= "VILL_CODE")
vor_villagesCH<-vor_villagesCH[,c(1:6,12:13)]
head(vor_villagesCH)
colnames(vor_villagesCH@data)<-c("VILL_CODE","SD_CODE","D_CODE","P_CODE","area_km","count", "X","Y" )

vor_villagesDK<-merge(vor_villages, pres_DKcountXY, by= "VILL_CODE")
vor_villagesDK<-vor_villagesDK[,c(1:6,12:13)]
head(vor_villagesDK)
colnames(vor_villagesDK@data)<-c("VILL_CODE","SD_CODE","D_CODE","P_CODE","area_km","count", "X","Y" )

## --------  AGGREGATION (pt2) of REgular boundaries census units ----
# Compute Average Spatial Resultion (ASR)
summary(vor_villagesCH@data$area_km) #mean area (km²) at thiessen polygon level:  4.8470km²
ASR_vil<-sqrt(sum(vor_villagesCH@data$area_km)/nrow(vor_villagesCH@data)) #3.11 km

thai_Subdistr@data$area_km<- gArea(thai_Subdistr, byid=TRUE)/1000000
ASR_subdist<-sqrt(sum(thai_Subdistr@data$area_km)/nrow(thai_Subdistr@data)) #8.33 km

thai_distr@data$area_km<- gArea(thai_distr, byid=TRUE)/1000000
ASR_distr<-sqrt(sum(thai_distr@data$area_km)/nrow(thai_distr@data)) #23.60 km

#create a vector grid
# Create an empty raster.
gridVil<- raster(extent(thai))
gridSubdist<- raster(extent(thai))
gridDist<- raster(extent(thai))

# Choose rasters resolution on ASR
res(gridVil) <- ASR_vil
res(gridSubdist) <- ASR_subdist
res(gridDist) <- ASR_distr

# Make the grid have the same coordinate reference system (CRS) as the shapefile.
proj4string(gridVil)<-proj4string(thai)
proj4string(gridSubdist)<-proj4string(thai)
proj4string(gridDist)<-proj4string(thai)

writeRaster(gridDist, "gridDist.tif")

#Polygonize this raster into a polygon and you will have a grid
gridpolygonVil <- rasterToPolygons(gridVil)
gridpolygonSubdist <- rasterToPolygons(gridSubdist)
gridpolygonDist <- rasterToPolygons(gridDist)

gridpolygonVil@data$id_cell<-rownames(gridpolygonVil@data)
gridpolygonSubdist@data$id_cell<-rownames(gridpolygonSubdist@data)
gridpolygonDist@data$id_cell<-rownames(gridpolygonDist@data)

head(gridpolygonVil@data)
head(gridpolygonSubdist@data)
head(gridpolygonDist@data)

#subset grids within Thai the borders
gridpolygonVil<-gridpolygonVil[thai,]
gridpolygonSubdist<-gridpolygonSubdist[thai,]
gridpolygonDist<-gridpolygonDist[thai,]

gridpolygonVil@data$area_km<- gArea(gridpolygonVil, byid=TRUE)/1000000
gridpolygonSubdist@data$area_km<- gArea(gridpolygonSubdist, byid=TRUE)/1000000
gridpolygonDist@data$area_km<- gArea(gridpolygonDist, byid=TRUE)/1000000

#ok now ze should have 3 gridded shapefile with one ID values for each cell. 
#Now we have to aggregate the values of the villages; SubDistr and Distr to the gridded version
#we have to do some saptial join and then aggregate the values 
library(spatialEco)
names(gridpolygonVil)
gridpolygonVil<-gridpolygonVil[,1]
crs(pres_CHcountXY)<-" +proj=utm +zone=47 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"
pts.poly <- point.in.poly(pres_CHcountXY, gridpolygonVil)
head(pts.poly@data)
chick_vill_grid<-aggregate(pts.poly$ALL_CHICKEN ~ pts.poly$id_cell,data=pts.poly,sum)
colnames(chick_vill_grid)<-c("id_cell", "count")

nrow(gridpolygonVil@data)
nrow(chick_vill_grid)
chick_vill_grid <- merge(gridpolygonVil, chick_vill_grid, by='id_cell')
chick_vill_grid@data$area_km<- gArea(chick_vill_grid, byid=TRUE)/1000000

gridpolygonSubdist<-gridpolygonSubdist[,1]
pts.poly <- point.in.poly(pres_CHcountXY, gridpolygonSubdist)
chick_Subdist_grid<-aggregate(pts.poly$count ~ pts.poly$id_cell,data=pts.poly,sum)
colnames(chick_Subdist_grid)<-c("id_cell", "count")
nrow(gridpolygonSubdist@data)
nrow(chick_Subdist_grid)
chick_Subdist_grid <- merge(gridpolygonSubdist, chick_Subdist_grid, by='id_cell')
chick_Subdist_grid@data$area_km<- gArea(chick_Subdist_grid, byid=TRUE)/1000000

gridpolygonDist<-gridpolygonDist[,1]
pts.poly <- point.in.poly(pres_CHcountXY, gridpolygonDist)
chick_Distr_grid<-aggregate(pts.poly$count ~ pts.poly$id_cell,data=pts.poly,sum)
colnames(chick_Distr_grid)<-c("id_cell", "count")
nrow(gridpolygonDist@data)
nrow(chick_Distr_grid)
chick_Distr_grid <- merge(gridpolygonDist, chick_Distr_grid, by='id_cell')
chick_Distr_grid@data$area_km<- gArea(chick_Distr_grid, byid=TRUE)/1000000
plot(chick_Distr_grid)


library(woodson)
chick_vill_grid@data<-data.table(chick_vill_grid@data)
chick_Subdist_grid@data<-data.table(chick_Subdist_grid@data)
chick_Distr_grid@data<-data.table(chick_Distr_grid@data)

wmap(chloropleth_map=chick_vill_grid, 
     geog_id="id_cell", 
     variable="count",
     chlor_lcol="white",
     histogram=TRUE,
     hist_color="grey",
     dist_stats=c("mean","sd"),
     map_title="CH Villages REG")

wmap(chloropleth_map=chick_Subdist_grid, 
     geog_id="id_cell", 
     variable="count",
     chlor_lcol="white",
     histogram=TRUE,
     hist_color="grey",
     dist_stats=c("mean","sd"),
     map_title="Sub-district")

wmap(chloropleth_map=chick_Distr_grid, 
     geog_id="id_cell", 
     variable="count",
     chlor_lcol="white",
     histogram=TRUE,
     hist_color="grey",
     dist_stats=c("mean","sd"),
     map_title="CH District REG")


wmap(chloropleth_map=CH_vill, 
     geog_id="ID", 
     variable="count",
     chlor_lcol="white",
     histogram=TRUE,
     hist_color="grey",
     dist_stats=c("mean","sd"),
     map_title="CH Villages IRR")

wmap(chloropleth_map=CH_subDistr, 
     geog_id="ID", 
     variable="count",
     chlor_lcol="white",
     histogram=TRUE,
     hist_color="grey",
     dist_stats=c("mean","sd"),
     map_title="Sub-district IRR")

wmap(chloropleth_map=CH_distr, 
     geog_id="ID", 
     variable="count",
     chlor_lcol="white",
     histogram=TRUE,
     hist_color="grey",
     dist_stats=c("mean","sd"),
     map_title="CH District IRR")


#lets do the same for ducks
library(spatialEco)
crs(pres_DKcountXY)<-" +proj=utm +zone=47 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"
pts.poly <- point.in.poly(pres_DKcountXY, gridpolygonVil)
head(pts.poly@data)
duck_vill_grid<-aggregate(pts.poly$ALL_DUCK ~ pts.poly$id_cell,data=pts.poly,sum)
colnames(duck_vill_grid)<-c("id_cell", "count")

nrow(gridpolygonVil@data)
nrow(duck_vill_grid)
duck_vill_grid <- merge(gridpolygonVil, duck_vill_grid, by='id_cell')
duck_vill_grid@data$area_km<- gArea(duck_vill_grid, byid=TRUE)/1000000

gridpolygonSubdist<-gridpolygonSubdist[,1]
pts.poly <- point.in.poly(pres_DKcountXY, gridpolygonSubdist)
duck_Subdist_grid<-aggregate(pts.poly$ALL_DUCK ~ pts.poly$id_cell,data=pts.poly,sum)
colnames(duck_Subdist_grid)<-c("id_cell", "count")
nrow(gridpolygonSubdist@data)
nrow(duck_Subdist_grid)
duck_Subdist_grid <- merge(gridpolygonSubdist, duck_Subdist_grid, by='id_cell')
duck_Subdist_grid@data$area_km<- gArea(duck_Subdist_grid, byid=TRUE)/1000000

gridpolygonDist<-gridpolygonDist[,1]
pts.poly <- point.in.poly(pres_DKcountXY, gridpolygonDist)
duck_Distr_grid<-aggregate(pts.poly$ALL_DUCK ~ pts.poly$id_cell,data=pts.poly,sum)
colnames(duck_Distr_grid)<-c("id_cell", "count")
nrow(gridpolygonDist@data)
nrow(duck_Distr_grid)
duck_Distr_grid <- merge(gridpolygonDist, duck_Distr_grid, by='id_cell')
duck_Distr_grid@data$area_km<- gArea(duck_Distr_grid, byid=TRUE)/1000000
plot(duck_Distr_grid)

#------- Write Response variable files ----
### Irregular boundaries
names(vor_villagesCH)
CH_vill<-vor_villagesCH[,c(1, 5:6)]
CH_vill$ID  <- 1:nrow(CH_vill)
names(CH_vill)

names(chick_subDistr)
names(thai_Subdistr)
CH_subDistr<-merge(thai_Subdistr, chick_subDistr, by= "SD_CODE")
CH_subDistr<-CH_subDistr[,c(1,18:19)]
CH_subDistr$ID  <- 1:nrow(CH_subDistr)
head(CH_subDistr)

names(chick_distr)
names(thai_distr)
CH_distr<-merge(thai_distr, chick_distr, by= "D_CODE")
CH_distr<-CH_distr[,c(1,7:8)]
CH_distr$ID  <- 1:nrow(CH_distr)
head(CH_distr)


# install.packages("devtools") # Only do this if you haven't already installed the devtools packages
# library(devtools) # This has the install_github function in it
# install_github("RebeccaStubbs/woodson")
library(woodson)

#before writing the shapefiles, lets check the topology
CH_vill@data<-data.table(CH_vill@data)
CH_subDistr@data<-data.table(CH_subDistr@data)
CH_distr@data<-data.table(CH_distr@data)

check_wmapshp(CH_vill)
check_wmapshp(CH_subDistr)
check_wmapshp(CH_distr)

dir.create("C:/Users/ddare/Documents/DanieleDaRe/UCL/Paper1/september2017/NewAggregations/Responses/CH_IRRrp", showWarnings = FALSE)
setwd("C:/Users/ddare/Documents/DanieleDaRe/UCL/Paper1/september2017/NewAggregations/Responses/CH_IRRrp")
shapefile(CH_vill, "CHvil_IRRrp.shp", overwrite=T)
shapefile(CH_subDistr, "CHsds_IRRrp.shp", overwrite=T)
shapefile(CH_distr, "CHsdis_IRRrp.shp",  overwrite=T)

#for ducks
names(vor_villagesDK)
DK_vill<-vor_villagesDK[,c(1, 5:6)]
DK_vill$ID  <- 1:nrow(DK_vill)
names(DK_vill)

names(duck_subDistr)
names(thai_Subdistr)
DK_subDistr<-merge(thai_Subdistr, duck_subDistr, by= "SD_CODE")
DK_subDistr<-DK_subDistr[,c(1,18:19)]
DK_subDistr$ID  <- 1:nrow(DK_subDistr)
head(DK_subDistr)

names(duck_distr)
names(thai_distr)
DK_distr<-merge(thai_distr, duck_distr, by= "D_CODE")
DK_distr<-DK_distr[,c(1,7:8)]
DK_distr$ID  <- 1:nrow(DK_distr)
head(DK_distr)

dir.create("/home/ddare/Documents/DanieleDaRe/UCL/Paper1/september2017/NewAggregations/Responses/DK_IRRrp", showWarnings = FALSE)
setwd("/home/ddare/Documents/DanieleDaRe/UCL/Paper1/september2017/NewAggregations/Responses/DK_IRRrp")
shapefile(DK_vill, "DKvil_IRRrp.shp", overwrite=T)
shapefile(DK_subDistr, "DKsds_IRRrp.shp", overwrite=T)
shapefile(DK_distr, "DKdis_IRRrp.shp",  overwrite=T)
setwd("/home/ddare/Documents/DanieleDaRe/UCL/Paper1/september2017/NewAggregations")

### Regular boundaries
dir.create("/home/ddare/Documents/DanieleDaRe/UCL/Paper1/september2017/NewAggregations/Responses/CH_REGrp", showWarnings = FALSE)
setwd("/home/ddare/Documents/DanieleDaRe/UCL/Paper1/september2017/NewAggregations/Responses/CH_REGrp")

chick_vill_grid@data<-data.table(chick_vill_grid@data)
chick_vill_grid<-chick_vill_grid[,c(1,5,2)]
chick_vill_grid$ID  <- 1:nrow(chick_vill_grid)

chick_Subdist_grid@data<-data.table(chick_Subdist_grid@data)
chick_Subdist_grid<-chick_Subdist_grid[,c(1,5,2)]
chick_Subdist_grid$ID  <- 1:nrow(chick_Subdist_grid)

chick_Distr_grid@data<-data.table(chick_Distr_grid@data)
chick_Distr_grid<-chick_Distr_grid[,c(1,5,2)]
chick_Distr_grid$ID  <- 1:nrow(chick_Distr_grid)

shapefile(chick_vill_grid, "CHvil_REGrp.shp", overwrite=T)
shapefile(chick_Subdist_grid, "CHsds_REGrp.shp", overwrite=T)
shapefile(chick_Distr_grid, "CHdis_REGrp.shp",  overwrite=T)

### ducks regular boundaries
dir.create("/home/ddare/Documents/DanieleDaRe/UCL/Paper1/september2017/NewAggregations/Responses/DK_REGrp", showWarnings = FALSE)
setwd("/home/ddare/Documents/DanieleDaRe/UCL/Paper1/september2017/NewAggregations/Responses/DK_REGrp")
#duck_vill_grid<-chick_vill_grid
#duck_vill_grid@data<-data.table(duck_vill_grid@data)
duck_vill_grid$ID  <- 1:nrow(duck_vill_grid)

#duck_Subdist_grid@data<-data.table(duck_Subdist_grid@data)
duck_Subdist_grid$ID  <- 1:nrow(duck_Subdist_grid)

#duck_Distr_grid@data<-data.table(duck_Distr_grid@data)
duck_Distr_grid$ID  <- 1:nrow(duck_Distr_grid)

shapefile(duck_vill_grid, "DKvil_REGrp.shp", overwrite=T)
shapefile(duck_Subdist_grid, "DKsds_REGrp.shp", overwrite=T)
shapefile(duck_Distr_grid, "DKdis_REGrp.shp",  overwrite=T)

setwd("/home/ddare/Documents/DanieleDaRe/UCL/Paper1/september2017/NewAggregations")

### create id raster
thai_rast<- raster(extent(thai))
res(thai_rast) <- c(1000,1000)
proj4string(thai_rast)<-proj4string(thai)

x<-rasterize(CH_vill,thai_rast, "ID")
writeRaster(x, "CHsvil_IRRrp.tif")

x<-rasterize(CH_subDistr,thai_rast, "ID")
writeRaster(x, "CHsds_IRRrp.tif")

x<-rasterize(CH_distr,thai_rast, "ID")
writeRaster(x, "CHsdis_IRRrp.tif")

setwd("/home/ddare/Documents/DanieleDaRe/UCL/Paper1/september2017/NewAggregations")

